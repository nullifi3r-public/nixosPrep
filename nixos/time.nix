{config, ...}: {
  time.timeZone = "Africa/Cairo";

  time.hardwareClockInLocalTime = false; # IF TRUE, KEEP THE HARDWARE CLOCK IN LOCAL TIME INSTEAD OF UTC

  ## NETWORK TIME PROTOCOLS
  services.ntp.enable = false;
  services.chrony.enable = false;
  services.timesyncd.enable = true; # UNLIKE OTHERS, THIS IS JUST A CLIENT AND DOESN'T INCLUDE A SERVER COMPONENT
}
