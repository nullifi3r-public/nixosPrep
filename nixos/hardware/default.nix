{...}:
{
  imports = [
  ./acceleration

  ./audio.nix
  ./bluetooth.nix
  ./input.nix
  ./keyd.nix
  ./power_optimizations.nix
  ./printing_scanning.nix
  ./ram.nix
  ./storage.nix
  ];

  hardware.enableAllFirmware = true;
}
