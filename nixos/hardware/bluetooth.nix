{
  config,
  lib,
  machineType,
  ...
}:
lib.mkIf ((machineType == "desktop") && (config.customOPTS.hw_services.bluetooth.enable)) {
  hardware.bluetooth = {
    enable = true;
    powerOnBoot = true;
  };

  services.blueman.enable = lib.mkIf (config.customOPTS.desktops.hyprland.enable) true;

  customOPTS.filesystems.persist.root.directories = ["/var/lib/bluetooth"];

  systemd = lib.mkIf (!lib.strings.hasInfix "impermanence" config.customOPTS.filesystems.osFS.format) {
    tmpfiles.rules = [ "d /var/lib/bluetooth 700 root root - -"  ];
    targets."bluetooth".after = ["systemd-tmpfiles-setup.service"];
  };
}
