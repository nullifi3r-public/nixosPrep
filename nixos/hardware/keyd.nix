{
  config,
  inputs,
  lib,
  ...
}:
lib.mkIf (config.customOPTS.hw_services.keyd.enable) {
  services.keyd = {
    enable = true;
  };

  #systemd.services.keyd.wantedBy = [ "multi-user.target" ]; # DEFAULT
  #systemd.services.keyd.before = [ "multi-user.target" ];

  environment.etc."keyd/default.conf" = {
    source = "${inputs.penv}/config/system/keyd/default.conf";
  };
}
