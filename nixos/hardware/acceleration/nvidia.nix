## WHEN NVIDIA CARD IS AVAILABLE:
## CHECK NVIDIA SUPPORT CONFIG IN ZANEY'S REPO @ https://gitlab.com/Zaney/zaneyos
## CHECK NVIDIA SUPPORT CONFIG IN Iynaix'S REPO @ https://github.com/iynaix/dotfiles

{ config, pkgs, lib, ... }:
lib.mkIf (config.customOPTS.hw_services.hwaccl.enable && config.customOPTS.hw_services.hwaccl.vendor == "nvidia") {}
