{ config, pkgs, lib, ... }:

lib.mkIf (config.customOPTS.hw_services.hwaccl.enable && config.customOPTS.hw_services.hwaccl.vendor == "intel") {
  hardware.graphics = {
    enable = true;
    enable32Bit = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      intel-ocl
      intel-vaapi-driver
      libvdpau-va-gl
      vaapiVdpau
    ];
  };
}
