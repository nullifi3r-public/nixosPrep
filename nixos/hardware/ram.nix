{config, ...}: {
  services.earlyoom = {
    enable = true;
    freeMemThreshold = 8;
    freeSwapThreshold = 8;
    freeMemKillThreshold = 4;
    freeSwapKillThreshold = 4;
    enableNotifications = true; # ANNOYING, BUT WE WANT TO KNOW WHAT'S KILLED
    extraArgs = let
      catPatterns = patterns: builtins.concatStringsSep "|" patterns;
      preferPatterns = [
        # DISPENSABLE PROCESSES
        "electron"
      ];
      avoidPatterns = [
        # THINGS WE WANT TO NOT KILL
        ".*firefox.*"
        ".*Hyprland.*"
        ".*plasma.*"
        ".*vivaldi.*"
        "asbru-cm"
        "kitty"
        "ssh"
        "sshd"
        ".*starman.*" ".*nix-serve"
        "systemd-logind"
        "systemd-udevd"
        "systemd"
      ];
    in [
      "-g"
      "--prefer '^(${catPatterns preferPatterns})$'"
      "--avoid '^(${catPatterns avoidPatterns})$'"
    ];
  };

  services.systembus-notify.enable = config.services.earlyoom.enableNotifications;
}
