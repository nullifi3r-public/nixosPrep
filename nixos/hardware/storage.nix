{
  config,
  lib,
  machineType,
  ...
}: {
  # ENABLE ENVFS | https://github.com/Mic92/envfs
  services.envfs.enable = true;

  # FIX USB STICKS NOT MOUNTING OR BEING LISTED:
  services.gvfs.enable = lib.mkIf (machineType != "server") true;
  services.devmon.enable = true;  # AUTOMOUNT DISKS
  services.udisks2.enable = true; # DBUS SERVICE THAT ALLOWS APPLICATIONS TO QUERY AND MANIPULATE STORAGE DEVICES

  services.fstrim = {
    enable = true;
    interval = "weekly";
  };
}
