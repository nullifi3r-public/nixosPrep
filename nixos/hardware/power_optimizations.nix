{
  config,
  isLaptop,
  lib,
  pkgs,
  ...
}:
lib.mkIf (isLaptop) {
  # Better scheduling for CPU cycles - thanks System76!!!
  services.system76-scheduler.settings.cfsProfiles.enable = true;

  # Enable TLP (better than gnomes internal power manager)
  services.tlp = lib.mkIf (!config.customOPTS.desktops.plasma.enable) {
    enable = true;
    settings = {
      CPU_SCALING_GOVERNOR_ON_BAT = "powersave";
      CPU_SCALING_GOVERNOR_ON_AC = "performance";

      CPU_ENERGY_PERF_POLICY_ON_BAT = "power";
      CPU_ENERGY_PERF_POLICY_ON_AC = "performance";

      CPU_BOOST_ON_AC = 1;
      CPU_BOOST_ON_BAT = 0;

      #CPU_MIN_PERF_ON_AC = 0;
      #CPU_MAX_PERF_ON_AC = 100;
      #CPU_MIN_PERF_ON_BAT = 0;
      #CPU_MAX_PERF_ON_BAT = 80;
    };
  };

  # Disable GNOMEs power management
  services.power-profiles-daemon.enable = lib.mkIf (config.services.tlp.enable) false; # Bad defaults and conflicts with TLP.

  # Enable powertop
  powerManagement.powertop.enable = true;

  # Enable thermald (only necessary if on Intel CPUs)
  services.thermald.enable = lib.mkIf (config.customOPTS.hw_services.hwaccl.vendor == "intel") true;
}
