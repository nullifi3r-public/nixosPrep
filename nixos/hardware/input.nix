{
  config,
  lib,
  isLaptop,
  ...
}:
lib.mkIf (isLaptop) {
  services.libinput = {
    enable = true;
    touchpad.tapping = true;
    touchpad.middleEmulation = true;
    touchpad.naturalScrolling = true;
    mouse.middleEmulation = true;
  };
}
