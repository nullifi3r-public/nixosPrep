{
  config,
  pkgs,
  lib,
  ...
}:
lib.mkIf (config.customOPTS.hw_services.printing_scanning.enable) {
  hardware.sane.enable = true; # NOT NEEDED UNDER VMs (OVERRIDEN)
  services.ipp-usb.enable = true; # NOT NEEDED UNDER VMs
  services.printing.enable = true; # NOT NEEDED UNDER VMs
  services.saned.enable = true;
  #services.saned.extraConfig = ''  '';

  services.udev.packages = [pkgs.utsushi];
  hardware.sane.extraBackends = with pkgs; [
    sane-airscan # APPLE/MICROSOFT AIRSCAN BACKEND
    epkowa # EPSON BACEND
    utsushi # UTSUSHI BACKEND
    hplipWithPlugin # HP BACKEND
  ];
}
