{ config, lib, user, ... }:
{
  environment = {
    
    variables = {  # SCOPE: SYSTEM
      PATH = [
        "/home/${user}/.local/bin"
        "/usr/local/bin"
        #"/home/${user}/.config/rofi/scripts"
      ];

      EDITOR = "vim";
      SUDO_EDITOR = "vim";
      SYSTEMD_EDITOR = "vim";

      NIXPKGS_ALLOW_UNFREE = "1";

      XDG_DATA_DIRS = lib.mkForce "/var/lib/flatpak/exports/share:$HOME/.local/share/flatpak/exports/share:$XDG_DATA_DIRS";
    };

    pathsToLink = [
      "/share/xdg-desktop-portal" "/share/applications"
      # "/share/bash-completion" "/share/fish" "/share/zsh"
    ];
  };
}
