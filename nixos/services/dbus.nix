{
  config,
  lib,
  ...
}: {
  
  boot.initrd.systemd.dbus.enable = true;
  # use dbus broker as the default implementation which aims to provide high performance and reliability, while keeping compatibility to the D-Bus reference implementation
  services = {
    dbus = {
      enable = true;
      implementation = "broker";
    };
    tumbler.enable = lib.mkIf (!config.customOPTS.isVM.enable) true; #  A D-Bus thumbnailer service
  };
}
