{config, pkgs, ...}:{
  services = {
    locate = {
      enable = true;
      localuser = null;
      interval = "hourly";
      package = pkgs.plocate;
    };
  };
}
