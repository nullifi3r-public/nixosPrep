{
  config,
  isLaptop,
  lib,
  machineType,
  pkgs,
  ...
}: {

  services = {
    colord.enable = lib.mkIf (machineType != "server") true; # /var/lib/colord IS INCLUDED IN THE MAIN IMPERMANENCE MODULE

    logind = lib.mkIf (isLaptop) {
      lidSwitch = "ignore";
      lidSwitchDocked = "ignore";
      lidSwitchExternalPower = "ignore";
      #extraConfig = ''      '';
    };

    xserver = {
      enable = if (config.customOPTS.desktops.display_manager == null) then true else false;
      #updateDbusEnvironment = true;
      xkb = { # CONFIGURE KEYBOARD KEYMAP IN X11
        layout = "us,ara";
        variant = "";
        #options = "caps:escape"; # <KEYD> IS IN THE TOWN NOW
      };
      excludePackages = [ pkgs.xterm ]; # BYE BYE XTERM
    };
  };
}
