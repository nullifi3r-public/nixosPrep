{
  config,
  lib,
  pkgs,
  ...
}:
lib.mkIf (config.customOPTS.desktops.plasma.enable) {
  services.desktopManager = {
    plasma6.enable = true;
    plasma6.enableQt5Integration = true;
  };
  environment.plasma6.excludePackages = [pkgs.kdePackages.elisa];
}
