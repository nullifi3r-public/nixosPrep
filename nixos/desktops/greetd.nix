{
  config,
  lib,
  machineType,
  pkgs,
  user,
  ...
}: lib.mkIf ((machineType != "server") && (config.customOPTS.desktops.display_manager == "greetd")) {
  environment.systemPackages = with pkgs; [greetd.tuigreet];
  services.displayManager.sddm.enable = false;
  services.greetd = {
    enable = true;
    vt = 5;
    settings = {
      default_session = lib.mkMerge [
        # GENERAL SETTINGS
        {
          user = user;
        }
/*
        # DEFAULT TO PLASMA IF ENABLED
        (lib.mkIf (config.customOPTS.desktops.plasma.enable) {
          command = "${pkgs.greetd.tuigreet}/bin/tuigreet --remember --remember-user-session --time --cmd startplasma-wayland";
        })
*/
        # DEFAULT TO HYPRLAND IF ENABLED
        (lib.mkIf (config.customOPTS.desktops.hyprland.enable) {
          command = "${pkgs.greetd.tuigreet}/bin/tuigreet --remember --remember-user-session --time --cmd Hyprland";
        })
/**/
      ];
    };
  };
}
