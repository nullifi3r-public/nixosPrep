{
  config,
  lib,
  machineType,
  pkgs,
  user,
  ...
}: lib.mkIf ((machineType != "server") && (config.customOPTS.desktops.display_manager == "sddm")) {
  /*environment.systemPackages = with pkgs; [custom.sddm-sugar-dark-theme libsForQt5.qt5.qtgraphicaleffects
    libsForQt5.qt5.qtquickcontrols2
    libsForQt5.qt5.qtbase
    libsForQt5.qt5.qtsvg
    where-is-my-sddm-theme
  ];*/
  services.displayManager.sddm = {
    enable = true;
    wayland.enable = true; # DEFAULT IS FALSE

    autoNumlock = true;
    enableHidpi = true;

    #theme = "sugar-dark";
    #theme = "where_is_my_sddm_theme";

    #settings = lib.mkIf (config.customOPTS.isVM.enable) {
    #  Autologin = {
    #    Session = "plasma.desktop";
    #    User = user;
    #  };
    #};
  };
}
