{...}:{
  imports = [
    ./display-services.nix
    ./greetd.nix
    ./hyprland.nix
    ./plasma.nix
    ./sddm.nix
  ];
}
