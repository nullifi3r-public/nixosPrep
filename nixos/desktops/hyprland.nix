{
  config,
  pkgs,
  lib,
  ...
}:
lib.mkIf (config.customOPTS.desktops.hyprland.enable) {
  programs.hyprland = {
    enable = true;
    xwayland.enable = true;
    systemd.setPath.enable = true;
    portalPackage = pkgs.xdg-desktop-portal-hyprland;
  };

  # set here as legacy linux won't be able to set these
  #hm.wayland.windowManager.hyprland.enable = true;

  # OPTIONAL, HINT ELECTRON APPS TO USE WAYLAND:
  environment.sessionVariables = {
    ### APP/PLATFORM-SPECIFIC
    NIXOS_OZONE_WL = lib.mkForce "1";
    MOZ_ENABLE_WAYLAND = lib.mkForce "1";
    #POLKIT_BIN = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
    # XDG VARIABLES
    XDG_CURRENT_DESKTOP = "Hyprland";
    XDG_SESSION_DESKTOP = "Hyprland";
    # WAYLAND COMPATIBILITY
    ### GUI TOOLKITS
    #SDL_VIDEODRIVER = "wayland,x11";
    #GDK_BACKEND = "wayland";
    #QT_QPA_PLATFORM = "wayland";
    #CLUTTER_BACKEND = "wayland";
    #XDG_SESSION_TYPE = "wayland";
    #QT_QPA_PLATFORMTHEME = "qt5ct";
    #QT_AUTO_SCREEN_SCALE_FACTOR = "1";
    #QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";
  };

  xdg.portal = {
    enable = true;
    extraPortals = [pkgs.xdg-desktop-portal-gtk];
  };
  security.pam.services.hyprlock = {};
  /*security.pam.services.swaylock = {
    text = ''
      auth include login
    '';
  };*/
}
