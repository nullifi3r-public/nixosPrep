{
  config,
  lib,
  ...
}:
{
  services.openssh = {
    enable = true;
    authorizedKeysInHomedir = true;
    allowSFTP = true;
    ports = [22];
    settings = {
      #StrictModes = false;
      PermitRootLogin = "no"; # ONE OF: "yes", "without-password", "prohibit-password", "forced-commands-only", "no"
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
    };
  };

  programs.ssh.startAgent = true;

  systemd.services.sshd.wantedBy = lib.mkForce [ "multi-user.target" ];

  customOPTS.filesystems.persist.root.files = [
    "/etc/ssh/ssh_host_ed25519_key.pub"
    "/etc/ssh/ssh_host_ed25519_key"
    "/etc/ssh/ssh_host_rsa_key.pub"
    "/etc/ssh/ssh_host_rsa_key"
  ];

}
