# THIS FILE ASSUMES YOU'RE INSTALLING USING THE CUSTOM BUILT ISO FOR YOS ;)
{
  config,
  lib,
  machineType,
  user,
  ...
}: {
  # silence warning about setting multiple user password options
  # https://github.com/NixOS/nixpkgs/pull/287506#issuecomment-1950958990
  options = {
    warnings = lib.mkOption {
      apply = lib.filter (
        w: !(lib.strings.hasInfix "The options silently discard others by the order of precedence" w)
      );
    };
  };

  config = lib.mkMerge [
    {
      users = {
        mutableUsers = false;
        users = {
          root = {
            initialPassword = "++";
            hashedPasswordFile = "/ps_root/etc/classified/userPasswords/root_pass";
          };
          ${user} = {
            isNormalUser = true;
            description = "Yahia.Jr";
            uid = 1000;
            group = "users";

            initialPassword = "++";
            hashedPasswordFile = "/ps_root/etc/classified/userPasswords/user_pass";
            extraGroups = [
              # SUDO ACCESS
              "wheel" "adm"

              # SYSTEM
              "systemd-journal" "users"

              # VIRTUALIZATION: VBOX
              "vboxsf" "vboxusers"

              # VIRTUALIZATION: KVM
              "kvm" "libvirt" "libvirtd" "libvirt-qemu" "qemu"

              # SOFTWARE
              "adbusers" "avahi" "colord" "docker" "flatpak" "keyd" "nextcloud" "plocate" "sambashare" "sshd"

              # HARDWARE ACCESS
              "audio" "bluetooth" "camera" "dialout" "disk" "input" "lp" "networkmanager" "plugdev" "scanner" "video"
            ] ++ (lib.optional (config.customOPTS.programs.sops.enable) config.users.groups.keys.name);
          };
        };
      };
    }

    (lib.mkIf (config.customOPTS.programs.sops.enable) (
      {
        sops.secrets = { #config.
          "users_passwords/y_num".neededForUsers = true;
          "users_passwords/r_glob".neededForUsers = true;
          "users_passwords/vm_pass".neededForUsers = true;
        };

        users.users = {
          root = {
            hashedPasswordFile = if (machineType == "vm") then (lib.mkForce config.sops.secrets."users_passwords/vm_pass".path) else (lib.mkForce config.sops.secrets."users_passwords/y_num".path);
            #openssh.authorizedKeys.keys = config.users.users.${user}.openssh.authorizedKeys.keys;
          };
          ${user} = {
            hashedPasswordFile = if (machineType == "vm") then (lib.mkForce config.sops.secrets."users_passwords/vm_pass".path) else (lib.mkForce config.sops.secrets."users_passwords/r_glob".path);
            openssh.authorizedKeys.keys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGNrlVEIsQUyHVnmP+a7h9W4U02yNBTZ/P4WTkig1lay YUniKey"];
          };
        };
      }
    ))
  ];
}
