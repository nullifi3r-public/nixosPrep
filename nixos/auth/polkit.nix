{
  config,
  pkgs,
  lib,
  ...
}:{
  /*systemd.user.services.polkit-gnome-authentication-agent-1 = {
    enable = if (config.customOPTS.desktops.hyprland.enable)
      then true
      else false;
    description = "polkit-gnome-authentication-agent-1";
    wantedBy = [ "graphical-session.target" ];
    wants = [ "graphical-session.target" ];
    after = [ "graphical-session.target" ];
    serviceConfig = {
      Type = "simple";
      ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
      Restart = "on-failure";
      RestartSec = 1;
      TimeoutStopSec = 10;
    };
  };*/

  security.polkit = {
    enable = true;

    extraConfig = ''
      // Allow users group to run the following actions without authentication
      polkit.addRule(
        function(action, subject) {
          if ( subject.isInGroup("users")
            && (
              action.id == "org.gnome.gparted"
              || action.id == "org.btfrs-assistant.pkexec.policy.run"
              || action.id == "org.freedesktop.login1.reboot"
              || action.id == "org.freedesktop.login1.reboot-multiple-sessions"
              || action.id == "org.freedesktop.login1.power-off"
              || action.id == "org.freedesktop.login1.power-off-multiple-sessions"
            )
          )
        {return polkit.Result.YES;}
        }
      );

      // ALLOW ANY ACTION if user is local
      /*
      polkit.addRule(function(action, subject) {
        if (
          subject.local
        )
        return "yes";
      });
      */

     /* THE FOLLOWING IS JUST FOR REFERENCE TO APPLY ON NON-NIXOS DISTROS AS IT'S INCLUDED BY DEFAULT ON NIXOS
      polkit.addRule(function(action, subject) {
        if (
          subject.isInGroup("networkmanager")
          && (
            action.id.indexOf("org.freedesktop.NetworkManager.") == 0
            || action.id.indexOf("org.freedesktop.ModemManager")  == 0
          )
        )
      { return polkit.Result.YES; }
      }*/
    '';
  };
}
