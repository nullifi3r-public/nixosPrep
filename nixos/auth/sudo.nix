{
  config,
  inputs,
  lib,
  user,
  ...
}:
lib.mkIf (user == "yahia") {
  # DO NOT HAVE DOT CHARACTER (.) IN THE FILE NAME AS IT'S NOT SUPPORTED BY @includedir
  environment.etc."sudoers.d/yahia" = {
    mode = "0440";
    source = "${inputs.penv}/config/system/sudo/yahia.sudo";
  };

  security.sudo = {
    enable = true;
    extraConfig = ''@includedir /etc/sudoers.d '';
  };
}
