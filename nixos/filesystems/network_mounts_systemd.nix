{
  config,
  lib,
  machineType,
  pkgs,
  user,
  ...
}:
lib.mkIf ((machineType != "vm") && (config.customOPTS.filesystems.network.mounts.enable)) {
  services.rpcbind.enable = true; # needed for NFS
  systemd.mounts = let
    commonMountOptions = {
      type = "nfs";
      mountConfig = {
        Options = ["defaults" "_netdev" "nofail" "noauto" "rsize=1048576" "wsize=1048576"];
      };
    };
  in [
    (commonMountOptions
      // {
        what = "10.0.0.27:/disks/inDisk/archive/ISOs";
        where = "/home/${user}/storageHub/network/ISOs";
      })

    (commonMountOptions
      // {
        what = "10.0.0.27:/disks/exDisk/downloads";
        where = "/home/${user}/storageHub/network/_downloads/remote_downloads";
      })

    (commonMountOptions
      // {
        what = "10.0.0.27:/disks/exDisk/apps/photoprism/photos";
        where = "/home/${user}/storageHub/network/gallery";
      })

    (commonMountOptions
      // {
        what = "10.0.0.27:/disks/exDisk/apps/nextcloud/data/${user}/files/Media_Libraries";
        where = "/home/${user}/storageHub/network/media_library";
      })
  ];

  systemd.automounts = let
    commonAutoMountOptions = {
      wantedBy = ["multi-user.target"];
      automountConfig = {
        TimeoutIdleSec = "300";
      };
    };
  in [
    (commonAutoMountOptions // {where = "/home/${user}/storageHub/network/_downloads/remote_downloads";})
    (commonAutoMountOptions // {where = "/home/${user}/storageHub/network/gallery";})
    (commonAutoMountOptions // {where = "/home/${user}/storageHub/network/ISOs";})
    (commonAutoMountOptions // {where = "/home/${user}/storageHub/network/media_library";})
  ];
}
