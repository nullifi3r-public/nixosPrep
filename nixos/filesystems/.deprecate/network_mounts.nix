{
  config,
  lib,
  machineType,
  pkgs,
  user,
  ...
}: let
  nfsOptions = {
    fsType = "nfs";
    options = ["x-systemd.automount" "x-systemd.idle-timeout=600" "defaults" "_netdev" "nofail" "noauto" "rsize=1048576" "wsize=1048576"];
  };
in lib.mkIf ((machineType != "vm") && (config.customOPTS.filesystems.network.mounts.enable)) {
  ## NETWORK (NFS)
  /*
  ** ADD "nfs-utils" TO THE SYSTEMD-WIDE INSTALLED PACKAGES
  ** ENABLE THIS SERVICE: "services.rpcbind.enable = true;"
  ** AND FOR ALLOWING NFS MOUNTS IN THE USERSPACE USE THE FOLLOWING WRAPPER
  */
  services.rpcbind.enable = true;
  security.wrappers."mount.nfs" = {
    setuid = true;
    owner = "root";
    group = "root";
    source = "${pkgs.nfs-utils.out}/bin/mount.nfs";
  };

  fileSystems."/home/${user}/storageHub/network/ISOs" = nfsOptions // {device = "10.0.0.27:/disks/inDisk/archive/ISOs";};
  fileSystems."/home/${user}/storageHub/network/_downloads/remote_downloads" = nfsOptions // {device = "10.0.0.27:/disks/exDisk/downloads";};
  fileSystems."/home/${user}/storageHub/network/gallery" = nfsOptions // {device = "10.0.0.27:/disks/exDisk/apps/photoprism/photos";};
  fileSystems."/home/${user}/storageHub/network/media_library" = nfsOptions // {device = "10.0.0.27:/disks/exDisk/apps/nextcloud/data/yahia/files/Media_Libraries";};
}
