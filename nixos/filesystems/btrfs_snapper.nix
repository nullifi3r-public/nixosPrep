{
  config,
  storageHub_state,
  lib,
  pkgs,
  user,
  ...
}: let
  sHub_mountpoint = "/home/${user}/storageHub";
  commonSnapperConfig = {
    ALLOW_USERS = ["${user}"];
    BACKGROUND_COMPARISON = false;
    EMPTY_PRE_POST_CLEANUP = true;
    EMPTY_PRE_POST_MIN_AGE = "1";
    FREE_LIMIT = "0.2";
    FSTYPE = "btrfs";
    NUMBER_CLEANUP = true;
    NUMBER_LIMIT = "4";
    NUMBER_LIMIT_IMPORTANT = "4";
    NUMBER_MIN_AGE = "1800";
    SPACE_LIMIT = "0.5";
    SYNC_ACL = true;
    TIMELINE_CLEANUP = true;
    TIMELINE_CREATE = true;
    TIMELINE_LIMIT_DAILY = 7;
    TIMELINE_LIMIT_HOURLY = 6;
    TIMELINE_LIMIT_MONTHLY = 1;
    TIMELINE_LIMIT_WEEKLY = 1;
    TIMELINE_LIMIT_YEARLY = 0;
    TIMELINE_MIN_AGE = 1800;
  };
in lib.mkMerge [
  ####################################
  ############ GENERAL
  ####################################
  (lib.mkIf (lib.strings.hasInfix "btrfs" config.customOPTS.filesystems.osFS.format) {
    environment.systemPackages = with pkgs; [btrfs-progs btrfs-assistant snapper];

    services.snapper = {
      cleanupInterval = "2h";
      snapshotInterval = "hourly";
    };
    
    services.btrfs.autoScrub = {
      enable = true;
      interval = "*-*-1/14"; # BI-WEEKLY
      #interval = "monthly";
      #fileSystems = [ "/" ]; # WILL AUTO DETECT MOUNTED BTRFS MOUNTPOINTS, NO NEED TO SPECIFY UNLESS YOU REALLY NEED TO.
    };
  })

  ####################################
  ############ SNAPPER
  ####################################
  # NOTE: ASSIGN THE SUBVOLUME ATTRIBUTE TO THE MOUNT POINT OF THE SUBVOLUME NOT THE NAME OF THE SUBVOLUME
  (lib.mkIf (config.customOPTS.filesystems.osFS.format == "btrfs") {
    services.snapper = {
      snapshotRootOnBoot = true;
      configs = {
        root = commonSnapperConfig // {SUBVOLUME = "/";};
        home = commonSnapperConfig // {SUBVOLUME = "/home";};
        sHub    = (lib.mkIf (storageHub_state != "n/a") (commonSnapperConfig // {SUBVOLUME = "/home/yahia/storageHub";}));
      };
    };

    systemd.services = {
      snapper-boot-home = {
        enable = true;
        description = "Take snapper snapshot of home on boot";
        requires = ["local-fs.target"];
        wantedBy = ["multi-user.target"];
        serviceConfig = {
          Type = "oneshot";
          ExecStart = "${pkgs.snapper}/bin/snapper --config home create --cleanup-algorithm number --description boot";
        };
        unitConfig.ConditionPathExists = "/etc/snapper/configs/home";
      };
      snapper-boot-sHub = {
        enable = if (storageHub_state != "n/a") then true else false;
        description = "Take snapper snapshot of storagHub on boot";
        requires = ["local-fs.target"];
        wantedBy = ["multi-user.target"];
        serviceConfig = {
          Type = "oneshot";
          ExecStart = "${pkgs.snapper}/bin/snapper --config sHub create --cleanup-algorithm number --description boot";
        };
        unitConfig.ConditionPathExists = ["/etc/snapper/configs/sHub" "${sHub_mountpoint}"];
      };
    };
  })

  (lib.mkIf (config.customOPTS.filesystems.osFS.format == "btrfs-impermanence") {
    services.snapper = {
      snapshotRootOnBoot = false;
      configs = {
        ps_root = commonSnapperConfig // {SUBVOLUME = "/ps_root";};
        ps_home = commonSnapperConfig // {SUBVOLUME = "/ps_home";};
        sHub    = (lib.mkIf (storageHub_state != "n/a") (commonSnapperConfig // {SUBVOLUME = "/home/yahia/storageHub";}));
      };
    };

    systemd.services = {
      snapper-boot-ps_root = {
        enable = true;
        description = "Take snapper snapshot of root (/ps_root) on boot";
        requires = ["local-fs.target"];
        wantedBy = ["multi-user.target"];
        serviceConfig = {
          Type = "oneshot";
          ExecStart = "${pkgs.snapper}/bin/snapper --config ps_root create --cleanup-algorithm number --description boot";
        };
        unitConfig.ConditionPathExists = "/etc/snapper/configs/ps_root";
      };
      
      snapper-boot-ps_home = {
        enable = true;
        description = "Take snapper snapshot of home (/ps_home) on boot";
        requires = ["local-fs.target"];
        wantedBy = ["multi-user.target"];
        serviceConfig = {
          Type = "oneshot";
          ExecStart = "${pkgs.snapper}/bin/snapper --config ps_home create --cleanup-algorithm number --description boot";
        };
        unitConfig.ConditionPathExists = "/etc/snapper/configs/ps_home";
      };
      snapper-boot-sHub = {
        enable = if (storageHub_state != "n/a") then true else false;
        description = "Take snapper snapshot of storagHub on boot";
        requires = ["local-fs.target"];
        wantedBy = ["multi-user.target"];
        serviceConfig = {
          Type = "oneshot";
          ExecStart = "${pkgs.snapper}/bin/snapper --config sHub create --cleanup-algorithm number --description boot";
        };
        unitConfig.ConditionPathExists = ["/etc/snapper/configs/sHub" "${sHub_mountpoint}"];
      };
    };
  })
]