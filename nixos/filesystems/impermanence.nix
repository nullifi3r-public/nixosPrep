{
  config,
  lib,
  user,
  ...
}: lib.mkIf (lib.strings.hasInfix "impermanence" config.customOPTS.filesystems.osFS.format){
  boot = lib.mkMerge [ # JUST PREPARING IN CASE I DECIDED TO STOP USING BTRFS
    (lib.mkIf (lib.strings.hasInfix "btrfs" config.customOPTS.filesystems.osFS.format) {
      ## MAKE SURE THE LV NAME MATCH THE ONE SET IN THE disko FILE !!!
      ### /dev/root_vg/(root) IS A LOGICAL-VOLUME
      ###  /btrfs_tmp/(@root) IS A BTRFS SUBVOLUME
      initrd.postDeviceCommands = lib.mkAfter ''
        mkdir -p "/btrfs_tmp"
        mount "/dev/root_vg/root" "/btrfs_tmp"
        if [[ -e '/btrfs_tmp/@root' ]]; then
            mkdir -p '/btrfs_tmp/old_roots'
            timestamp=$(date --date="@$(stat -c %Y '/btrfs_tmp/@root')" "+%Y-%m-%-d_%H:%M:%S")
            mv '/btrfs_tmp/@root' '/btrfs_tmp/old_roots/@root'-"$timestamp"
        fi

        delete_subvolume_recursively() {
            IFS=$'\n'
            for i in $(btrfs subvolume list -o "$1" | cut -f 9- -d ' '); do
                delete_subvolume_recursively "/btrfs_tmp/$i"
            done
            btrfs subvolume delete "$1"
        }

        for i in $(find /btrfs_tmp/old_roots/ -maxdepth 1 -mtime +10); do
            delete_subvolume_recursively "$i"
        done

        btrfs subvolume create '/btrfs_tmp/@root'
        umount '/btrfs_tmp'
      '';
    })

    # IN CASE I DECIDED TO LEAVE BTRFS
    (lib.mkIf (lib.strings.hasInfix "zfs" config.customOPTS.filesystems.osFS.format) {})

    (lib.mkIf (lib.strings.hasInfix "tmpfs" config.customOPTS.filesystems.osFS.format) {})
  ];

  environment.persistence = let
    # ALWAYS DEFINE OWNERSHIP & PERMISSONS FOR HOME DIRECTORIES & FILES
    generateHomeDirectories = map (str: { directory = str; user = "${user}"; group = "users"; mode = "u=rwx,g=rx,o=rx";});
    generateHomeFiles = map (str: { file = str; parentDirectory = { user = "${user}"; group = "users"; mode = "0755"; };});
  in {
    # CACHE PERSISTENCE
    "/ps_cache" = {
      hideMounts = true;
      files = config.customOPTS.filesystems.persist.root_cache.files;

      directories = (/*FLATPAK*/ lib.optionals (config.customOPTS.programs.flatpak.enable) ["/var/cache/flatpak"])
        ++ config.customOPTS.filesystems.persist.root_cache.directories;
      
      users.${user} = {
        files = generateHomeFiles config.customOPTS.filesystems.persist.home_cache.files
          ++ generateHomeFiles config.hm.customOPTS.filesystems.persist.home_cache.files;

        directories = [ { directory = ".cache"; user = "${user}"; group = "users"; mode = "u=rwx,g=rx,o=rx";} ]
          ++ generateHomeDirectories config.customOPTS.filesystems.persist.home_cache.directories
          ++ generateHomeDirectories config.hm.customOPTS.filesystems.persist.home_cache.directories;
      };
    };

    # HOME PERSISTENCE
    "/ps_home" = {
      hideMounts = true;
      users.${user} = {
        files = generateHomeFiles config.customOPTS.filesystems.persist.home.files
          ++ generateHomeFiles config.hm.customOPTS.filesystems.persist.home.files;

        directories = [
          ".config/dconf"
          ".config/xsettingsd"
          ".local/bin"
          ".local/share/applications"
          ".local/share/keyrings" ".pki"
        ] ++ /*WIREPLUMBER STATE*/ lib.optionals (config.services.pipewire.wireplumber.enable) [".local/state/wireplumber"]
          ++ /*SOPS*/ lib.optionals (config.customOPTS.programs.sops.enable) [".config/sops"]
          ++ generateHomeDirectories config.customOPTS.filesystems.persist.home.directories
          ++ generateHomeDirectories config.hm.customOPTS.filesystems.persist.home.directories;
      };
    };

    # ROOT PERSISTENCE
    "/ps_root" = {
      hideMounts = true;
      files = [
        "/etc/machine-id"
        #{ file = "/var/keys/secret_file"; parentDirectory = { mode = "u=rwx,g=,o="; }; }
      ] ++ config.customOPTS.filesystems.persist.root.files;

      directories = [
        "/etc/NetworkManager"
        "/var/lib/nixos"
        "/var/lib/systemd/coredump"
        "/var/log"
        { directory = "/var/lib/colord"; user = "colord"; group = "colord"; mode = "u=rwx,g=rx,o=";}
      ] ++ /*FLATPAK*/ lib.optionals (config.customOPTS.programs.flatpak.enable) ["/etc/flatpak" "/var/lib/flatpak" "/usr/share/flatpak"]
        ++ config.customOPTS.filesystems.persist.root.directories ;
    };
  };
}
