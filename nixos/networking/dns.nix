{
  config,
  lib,
  ...
}: {
  services.resolved = {
    enable = true;
    dnsovertls = lib.mkIf (config.networking.nameservers != []) "true"; # ONLY ENABLE DOT IF "config.networking.nameservers" LIST IS POPULATED
    fallbackDns = [
      "1.1.1.3"
      "1.0.0.3"
    ];
  };

  #networking.nameservers = [ # NAMESERVERS (GRAB CONNECTION'S DNS BY DEFAULT)
  #  "45.90.28.0#63857f.dns.nextdns.io"
  #  "45.90.30.0#63857f.dns.nextdns.io"
  #  "2a07:a8c0::#63857f.dns.nextdns.io"
  #  "2a07:a8c1::#63857f.dns.nextdns.io"
  #];
}
