{ config, ... }:{
  networking.proxy = { # PROXY CONFIGURATION (TYPICALLY NOT NEEDED), AND IF NEEDED IT WITH CREDENTIALS, USE SOPS FOR THEM
    default = "http://user:password@proxy:port/";
    noProxy = "127.0.0.1,localhost,internal.domain";
  };
}
