{config, host, ...}:{
  imports = [
    ./avahi.nix
    ./dns.nix
    ./filesharing.nix
    ./firewall.nix
    ./hosts.nix
    #./proxy.nix
  ];

  networking = {
    domain = "local";
    hostName = "${host}";
    networkmanager.enable = true;
  };
}
