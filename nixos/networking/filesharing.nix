{
  config,
  host,
  lib,
  ...
}:
lib.mkMerge [
  (lib.mkIf (config.customOPTS.filesystems.network.fileServers.smb.enable) {services.samba.enable = true;})

  (lib.mkIf (config.customOPTS.filesystems.network.fileServers.nfs.enable) {
    services.nfs.server = {
      enable = true;
      exports = lib.mkIf (host == "toshiba-homelab") ''
        ## InDisk
        /disks/inDisk/archive/ISOs              *(rw,fsid=0,sync,no_subtree_check,all_squash) #anonuid=997,anongid=997
        /disks/inDisk/archive/academic_library  *(rw,fsid=0,sync,no_subtree_check,all_squash) #anonuid=997,anongid=997
        ## ExDisk
        /disks/exDisk/downloads                 *(rw,fsid=1,sync,no_subtree_check,all_squash) #anonuid=997,anongid=997
        /disks/exDisk/apps/photoprism/photos    *(rw,fsid=1,sync,no_subtree_check,all_squash) #anonuid=992,anongid=33
        /disks/exDisk/apps/nextcloud/data/yahia/files/Media_Libraries   *(rw,fsid=1,sync,no_subtree_check,all_squash) #anonuid=997,anongid=997
      '';
    };
  })
]
