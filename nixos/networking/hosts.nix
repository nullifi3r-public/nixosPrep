{
  config,
  inputs,
  lib,
  machineType,
  ...
}: let
  sb_hosts_gambling_porn = builtins.readFile "${inputs.stevenblack-hosts}/alternates/gambling-porn/hosts";
in {
  networking = {
    #hosts = {
    #  "127.0.0.1" = [ "foo.bar.baz" ];
    #  "192.168.0.2" = [ "fileserver.local" "nameserver.local" ];
    #};
    extraHosts = ''
      ${sb_hosts_gambling_porn}
    '';
  };
}
