{
  config,
  lib,
  ...
}: let
  portsAllowedSingles = [
    22      # SSH
    80      # HTTP
    443     # HTTPS
    631     # PRINTER SHARING
    4001    # NOMACHINE
    24800   # BARRIER/FORKS
    55555   #aria2cd
  ]
  ++ lib.optionals (config.customOPTS.filesystems.network.fileServers.nfs.enable) [111  2049 4000 4001 4002 20048 ]; # NFS

  portsAllowedRanges = 
    lib.optionals (config.customOPTS.programs.docker.enable) [{from = 9000; to = 9999;}] # RANGE FOR DOCKER CONTAINERS
    ++ lib.optionals (config.programs.kdeconnect.enable) [{from = 1714; to = 1764;}] # KDE-CONNECT
    ++ lib.optionals (config.customOPTS.filesystems.network.fileServers.smb.enable) [{from = 137; to = 138;}]; # SAMBA
in {
  networking.firewall = {
    enable = if (config.customOPTS.isVM.enable) then false else true;
    allowPing = true;
    allowedTCPPorts = portsAllowedSingles;
    allowedUDPPorts = portsAllowedSingles;
    allowedTCPPortRanges = portsAllowedRanges;
    allowedUDPPortRanges = portsAllowedRanges;
  };
}
