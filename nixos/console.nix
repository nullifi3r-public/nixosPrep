{
  config,
  lib,
  pkgs,
  ...
}: {
  console = {
    colors = [
      "121314" # BASE00
      "4B3F2C" # OVERRIDDEN
      "B8CC52" # BASE0B
      "666D78" # BASE03
      "D58742" # BASE04
      "FFFFFF" # BASE05
      "D19A66" # BASE06
      "FFFFFF" # BASE07
      "E06C75" # BASE08
      "D19A66" # BASE09
      "FFB454" # BASE0A
      "B8CC52" # BASE0B
      "FF8F40" # BASE0C
      "6095D5" # BASE0D
      "D1AEDC" # BASE0E
      "3778A6" # BASE0F
    ];
    earlySetup = true;
    packages = [ pkgs.terminus_font ];
    font = lib.mkDefault "${pkgs.terminus_font}/share/consolefonts/ter-u24b.psf.gz";
    #keyMap = "us";
    useXkbConfig = true; # use xkbOptions in tty.
  };
}
