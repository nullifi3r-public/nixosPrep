{
  config,
  lib,
  pkgs,
  storageHub_state,
  user,
  ...
}: lib.mkIf (storageHub_state != "n/a") {
  system.activationScripts = {
    storagHubMGR.text = let 
      sHub_mountpoint = "/home/${user}/storageHub";
      device_to_mount = if (storageHub_state == "external") then "/dev/disk/by-partlabel/storageHub" else "/dev/root_vg/root";
    in ''
      create_sHub_and_fix_perms() {
        mkdir -p "${sHub_mountpoint}"
        chown "${user}":users "${sHub_mountpoint}"
        mount -t btrfs -o "subvol=@storageHub" -o "compress=zstd" -o "noatime" "${device_to_mount}" "${sHub_mountpoint}"
        chown "${user}":users "${sHub_mountpoint}"
      }

      shopt -s dotglob

      if [ -d "${sHub_mountpoint}" ]; then
        if ! mountpoint -q "${sHub_mountpoint}" ; then  
          if [ -z "$(ls -A "${sHub_mountpoint}")" ]; then
            rm -rf "${sHub_mountpoint}" && create_sHub_and_fix_perms
          else
            [ -d "/home/${user}/.storageHub.old" ] && "${pkgs.rsync}/bin/rsync" -av --remove-source-files "${sHub_mountpoint}/" "/home/${user}/.storageHub.old/" && rm -rf "${sHub_mountpoint}" || mv "${sHub_mountpoint}" "/home/${user}/.storageHub.old"
          fi
        else
          if [ $(df -T ${sHub_mountpoint} | cut -d " " -f2 | tr --delete "\n") = "btrfs" ]; then
            chown "${user}":users "${sHub_mountpoint}"
          fi
        fi
      else
        create_sHub_and_fix_perms
      fi

      if [ -d "${sHub_mountpoint}/.YOS" ]; then
        if mountpoint -q "${sHub_mountpoint}/.YOS" ; then
          chown -R "${user}":users "${sHub_mountpoint}/.YOS"
        fi
      fi

      if [ -d "${sHub_mountpoint}/guests" ]; then
        "${pkgs.e2fsprogs}/bin/chattr" -R +C "${sHub_mountpoint}/guests"
      fi
    '';
  };
}