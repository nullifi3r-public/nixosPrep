{
  config,
  lib,
  user,
  ...
}: lib.mkIf (lib.strings.hasInfix "impermanence" config.customOPTS.filesystems.osFS.format){
  system.activationScripts = {
    homePersistencePermssionMGR.text =  ''
      chown -R "${user}":users /ps_home/home
    '';
  };
}