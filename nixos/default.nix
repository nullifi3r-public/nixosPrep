{config, ...}: {
  imports = [
    ./auth
    ./bootloader
    ./desktops
    ./filesystems
    ./hardware
    ./networking
    ./programs
    ./services
    ./system-activation-scripts
    ./theming

    ./console.nix
    ./environment.nix
    ./locale.nix
    ./time.nix
  ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?
}
