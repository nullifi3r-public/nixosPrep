{
  bootloader,
  config,
  lib,
  ...
}:
lib.mkIf (bootloader == "systemd") {
  boot.loader = {
    efi.canTouchEfiVariables  = true;
    systemd-boot.enable       = true;
    timeout                   = 1;
  };
}
