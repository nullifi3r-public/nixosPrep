{config, ...}: {
  imports = [
    ./grub.nix
    ./systemd.nix
  ];

  config = {
    ## SYSTEMD-SERVICES TIMEOUT
    systemd.extraConfig = ''DefaultTimeoutStopSec=15s'';

    boot = {
      # enable stage-1 bootloader
      #initrd.systemd.enable = true;

      plymouth.enable = false; # BECAUSE IT'S NOT NEEDED & (I LOVE DETAILS)
      tmp.cleanOnBoot = true;

      #kernelParams = [
      # "resume=LABEL=/dev/disk/by-label/NIXSWAP"
      # "mem_sleep_default=disk" # If you want sleep to act like hibernate change "deep" to "disk" | reference: https://www.kernel.org/doc/Documentation/power/states.txt
      #];

      kernelModules = ["v4l2loopback"]; # THIS IS FOR VIRTUAL CAM SUPPORT - V4L2LOOPBACK SETUP
      extraModulePackages = [config.boot.kernelPackages.v4l2loopback];

      kernel = {
        sysctl = {
          "vm.swappiness" = 40;
        };
      };

      supportedFilesystems = [
        "btrfs"
        "ext4"
        "ntfs"
        "vfat"
        "xfs"
        #"zfs" # DEPENDANT ON OTHER COMPONENT TO BE ENABLED HERE, DON'T BOTHER IF YOU'RE NOT USING THIS FS
        # NETWORK FILESYSTEMS (NOT NEEDED FOR ANY USECASE CURRENTLY)
        #"nfs"
        #"cifs"
      ];
    };
  };
}
