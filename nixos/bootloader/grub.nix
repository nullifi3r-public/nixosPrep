# WHENEVER THIS FILE CHANGES, ADD THIS FLAG TO YOUR REBUILD COMMAND: --install-bootloader
{
  bootloader,
  config,
  lib,
  pkgs,
  ...
}: let 
  grupOptions = {
    enable                = true;
    enableCryptodisk      = false;

    ## APPEARANCE
    font                  = "${pkgs.hack-font}/share/fonts/truetype/Hack-Regular.ttf";
    fontSize              = 26;
    splashImage           = null;
    theme                 = pkgs.custom.distro-grub-themes-nixos;

    ## MENU ENTRIES
    configurationLimit    = 100; # (DEFAULT: 100)
    useOSProber           = false;
    memtest86.enable      = false; # REPLACED BY THE LIVE ISO
    extraEntries          = ''
      submenu "Power Options" {
        menuentry "Reboot" {
          reboot
        }

        menuentry "Poweroff" {
          halt
        }

        menuentry "UEFI Firmware Settings" {
          fwsetup
        }
      }
    '';
  };
in lib.mkIf (lib.lists.elem bootloader ["grub-bios" "grub-efi"]) {
  boot.loader = {
    timeout                   = 1;
    efi.canTouchEfiVariables  = lib.mkIf (bootloader == "grub-efi") true;
    grub                      = lib.mkMerge [    
      (lib.mkIf (bootloader == "grub-efi") grupOptions // {
        devices = ["nodev"];
        efiSupport = true;
      })
      
      (lib.mkIf (bootloader == "grub-bios") grupOptions // {
        ## FOR grub.device USE DEVICE NAME (DON"T USE LABEL/UUID) E.G.: /dev/sda
        device = config.disko.devices.disk.main.device;
      })
    ];
  };
}
