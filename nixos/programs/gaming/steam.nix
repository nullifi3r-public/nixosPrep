{
  config,
  lib,
  pkgs,
  ...
}: lib.mkIf (config.customOPTS.programs.gaming.steam.enable) {
  programs.steam = {
    enable = true;
    dedicatedServer.openFirewall = true; # OPEN PORTS IN THE FIREWALL FOR SOURCE DEDICATED SERVER
    gamescopeSession.enable = true;
    protontricks.enable = true; # A SIMPLE WRAPPER FOR RUNNING WINETRICKS COMMANDS FOR PROTON-ENABLED GAMES
    remotePlay.openFirewall = true; # OPEN PORTS IN THE FIREWALL FOR STEAM REMOTE PLAY
  };
  #environment.systemPackages = with pkgs; [ steam ]; # WILL INSTALL WITH HOME-MANAGER
}
