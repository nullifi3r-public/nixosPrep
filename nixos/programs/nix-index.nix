{ # REF: https://youtu.be/sLGoWAGklds
  config,
  lib,
  pkgs,
  ...
}: lib.mkIf (config.customOPTS.programs.nix-index.enable && !config.customOPTS.programs.command-not-found.enable) {
  # environment.variables = {
  #   NIX_AUTO_RUN = "1"; # IF ONLY 1 PACKAGE FOUND RELATED TO THE PACKAGE WRITTEN, RUN IT AUTOMATICALLY WITHOUT INTERACTIVE PROMPT
  #   NIX_AUTO_INSTALL = "1";
  # };

  programs = {
    command-not-found.enable = lib.mkForce false;
    nix-index = {
      enable = true;
      package = pkgs.nix-index;
      enableBashIntegration = true;
      enableFishIntegration = true;
      enableZshIntegration = true;
    };
  };
}
