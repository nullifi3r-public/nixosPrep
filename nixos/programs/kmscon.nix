{
  config,
  lib,
  pkgs,
  ...
}: {
  services.kmscon = {
    enable = true;

    extraConfig = ''
      font-size=14
      palette=custom

      palette-background=18,19,20
      palette-foreground=255,255,255

      palette-black=18,19,20
      palette-white=255,255,255

      palette-red=224,108,117
      palette-green=184,204,82
      palette-yellow=255,180,84
      palette-blue=96,149,213
      palette-magenta=209,174,220
      palette-cyan=255,143,64
      palette-light-grey=255,255,255
      palette-dark-grey=102,109,120
      palette-light-red=224,108,117
      palette-light-green=184,204,82
      palette-light-yellow=255,180,84
      palette-light-blue=96,149,213
      palette-light-magenta=209,174,220
      palette-light-cyan=255,143,64
    '';

    extraOptions = "--term 'xterm-256color' --session-max '0' --session-control --sb-size 50000";
    hwRender = config.customOPTS.hw_services.hwaccl.enable;
    fonts = [
      {
        name = "JetBrainsMono Nerd Font Mono";
        package = pkgs.nerdfonts.override {fonts = ["JetBrainsMono"];};
      }
    ];
  };
  environment.systemPackages = [ pkgs.kmscon ];
}
