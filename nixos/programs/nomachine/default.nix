{config, lib, ...}: {
  imports = [ ./nomachine.nix ];

  services.nxserver = lib.mkIf (config.customOPTS.programs.nomachine.enable) {
    enable = true;
    openFirewall = true;
  };
}
