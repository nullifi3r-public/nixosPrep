{...}: {
  imports = [
    ./gaming
    ./nomachine
    ./virtualisation

    ./_common.nix

    ./appimage.nix
    ./chromium_policies.nix
    ./command-not-found.nix
    ./kmscon.nix
    ./nix-index.nix
    ./nix-serve.nix
    ./nix.nix
  ];
}
