{
  config,
  host,
  lib,
  machineType,
  pkgs,
  self,
  user,
  ...
}: {
  ## DOCUMENTATION
  documentation = {
    nixos.enable = if (machineType == "desktop") then true else false;
    nixos.includeAllModules = config.documentation.nixos.enable;
    man.generateCaches = false; # THIS IS AUTO TRIGGERED ON EACH REBUILD, DON'T RUN "mandb" MANUALLY
  };

  ## SYSTEM-WIDE CONFIGURATIONS
  system = {
    ## NON-FLAKED SETUPS (CONFIG BACKUP & AUTO UPGRADE)
    #copySystemConfiguration = false;    # CLASSIC ONLY | DON'T USE WITH FLAKE SETUPS
    #autoUpgrade = { enable = true; allowReboot = true; channel = "https://nixos.org/channels/nixos-23.11";};

    ## BETTER NIXOS GENERATION LABEL: https://reddit.com/r/NixOS/comments/16t2njf/small_trick_for_people_using_nixos_with_flakes/k2d0sxx/
    nixos.label = lib.concatStringsSep "-" ((lib.sort (x: y: x < y) config.system.nixos.tags) ++ ["${config.system.nixos.version}.${self.sourceInfo.shortRev or "dirty"}"]);
  };

  ## PACKAGE MANAGER (nix)
  nix = let
    nixPath = [
      "nixpkgs=flake:nixpkgs"
      "/nix/var/nix/profiles/per-user/root/channels"
    ];
  in {
    inherit nixPath; # REQUIRED FOR nix-shell -p TO WORK
    channel.enable = false;
    ## BETTER NIX PERFORMANCE
    package = pkgs.nixVersions.latest;
    registry = {
      #nixpkgs.flake = inputs.nixpkgs;
      nixpkgs-master = {
        from = {
          type = "indirect";
          id = "nixpkgs-master";
        };
        to = {
          type = "github";
          owner = "NixOS";
          repo = "nixpkgs";
        };
      };
    };

    ## GENERAL SETTINGS
    settings = {
      warn-dirty = true;
      #eval-cache = false;
      nix-path = nixPath;
      auto-optimise-store = true;

      # removes ~/.nix-profile and ~/.nix-defexpr
      use-xdg-base-directories = true;
      experimental-features = ["nix-command" "flakes"];

      substituters = [
        "https://hyprland.cachix.org"
        "https://nix-community.cachix.org"
        "https://nix-gaming.cachix.org"
        "https://nixpkgs-wayland.cachix.org"
      ];
      trusted-public-keys = [
        "nix-cache.local-1:cOyMKqbEHdS58jshGxG49U7FdrXN9ijqeeR/7XNX0NY="
        "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        "nix-gaming.cachix.org-1:nbjlureqMbRAxR1gJ/f3hxemL9svXaZF/Ees8vCUUs4="
        "nixpkgs-wayland.cachix.org-1:3lwxaILxMRkVhehr5StQprHdEo4IrE8sRho9R9HOLYA="
      ];

      allowed-users = [user];
      trusted-users = [user];
    };

    ## GARBAGE-COLLECTION SETTINGS
    gc = {
      automatic = true;
      dates = "daily";
      options = "--delete-older-than 7d";
    };
  };

  ## CUSTOM SHELL PACKAGES 
  customOPTS.programs.shell.packages =  let 
    YOSFlakePath = "/home/${user}/storageHub/.YOS/nix";
    local_nix_cache = if (lib.lists.elem machineType ["desktop" "server"]) then "toshiba.local" else "cache-vm.local";
  in {
    vvx = ''nvim "${YOSFlakePath}"'';
    vx = ''vim "${YOSFlakePath}"'';
    upc = ''nix copy --all --to ssh://yahia@"${local_nix_cache}"'';
    rsss = '' clear; sudo nixos-rebuild switch --flake "${YOSFlakePath}/.#${host}"'';
    rass = ''
      if [ -d ${YOSFlakePath} ]; then
        if ping -c 1 "${local_nix_cache}" > /dev/null 2>&1; then
          clear; echo -e "\nLOCAL CACHE IS ONLINE, BUILDING...\n\n"

export NIX_CONFIG
NIX_CONFIG=$(cat <<'CONFIG'
extra-substituters = http://${local_nix_cache}:5000
trusted-substituters = http://${local_nix_cache}:5000
CONFIG
)

          #export NIX_CONFIG="extra-substituters = http://${local_nix_cache}:5000\n";
          nh os switch -H "${host}" "${YOSFlakePath}" && echo -e "\nPUSHING NIX STORE TO THE LOCAL CACHE...\n" && \
          nix copy -v --all --to ssh://yahia@${local_nix_cache}
          #nix copy -v  --substitute-on-destination --to http://${local_nix_cache}:5000 /run/current-system
          # sudo nixos-rebuild --option extra-substituters "http://${local_nix_cache}:5000" switch --flake "${YOSFlakePath}/.#${host}" && \
        else
          clear; nh os switch -H "${host}" "${YOSFlakePath}" && \
          echo -e "\n\n Remember to Push Your Local Nix Store to Your Hosted Local Cache (IF AVAILABLE!)\n"
        fi
      else
        echo -e "\nSTANDARD FLAKE DIRECTORY COULDN'T BE FOUND, GO TO THE FLAKE DIRECTORY AND RUN rmss COMMAND INSTEAD\n"
        exit 1
      fi
    '';
    rmss = ''
        clear; nh os switch -H "${host}" .
    '';

    # SET THE CURRENT GENERATION OR GIVEN GENERATION NUMBER AS DEFAULT TO BOOT
    setboot = ''
        if [ "$#" -eq 0 ]; then
          sudo /run/current-system/bin/switch-to-configuration boot
        else
          sudo "/nix/var/nix/profiles/system-$1-link/bin/switch-to-configuration" boot
        fi
      '';
  };
}
