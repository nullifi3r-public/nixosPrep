{
  config,
  inputs,
  lib,
  machineType,
  ...
}:
# https://chromeenterprise.google/policies/
lib.mkIf (machineType != "server") {
  environment.etc."chromium/policies/managed/forced.json" = {
    enable = true;
    source = "${inputs.penv}/config/user/browsers/chromium/policies.json";
  };
}
