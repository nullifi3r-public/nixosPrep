{
  config,
  inputs,
  lib,
  machineType,
  pkgs,
  ...
}:

{
  # ADD CUSTOM USER CREATED SHELL PACKAGES TO pkgs.custom.shell
  nixpkgs.overlays = [
    (_: prev: {
      custom = (prev.custom or { }) // {
        shell = config.customOPTS.programs.shell.finalPackages // config.hm.customOPTS.programs.shell.finalPackages;
      };
    })
  ];

  xdg.portal = lib.mkIf (config.customOPTS.desktops.display_manager != null) {
    enable = true;
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };

  programs = {
    adb.enable = lib.mkIf (machineType != "vm") true;
    dconf.enable = lib.mkIf (!config.customOPTS.desktops.plasma.enable) true;
  };

  environment = {
    # INSTALL GTK THEME FOR ROOT, SOME APPS LIKE GPARTED ONLY RUN AS ROOT
    #etc = lib.mkIf (machineType != "server") {"xdg/gtk-3.0/settings.ini".text = config.hm.xdg.configFile."gtk-3.0/settings.ini".text;};
    systemPackages = with pkgs; # DON'T INSTALL STANDARD TOOLS LIKE "curl" WITH HOME-MANAGER
    # UNIVERSAL PKGS
    [
      ### HARDWARE MGMT
      cifs-utils
      ddcutil
      exfat exfatprogs
      fuseiso  fuse-common
      parted
      libevdev
      ntfs3g
      udiskie
      v4l-utils

      ### SHELL UTILS
      comma # RUN NIXPKGS DIRECTLY ON THE COMMAND LINE
      killall
      lsof
      nh
      rsync
      tmux
      vim
      
      ### UTILITIES
      git
      gzip
      libnotify
      procps
      unzip
      zip
      
      ### NETWORK UTILITIES
      cacert
      curl
      nfs-utils
      openssl
      wget

      ## MEDIA UTILITIES
      ### VIDEO INFO PARSERS
      opencl-clang clinfo virglrenderer virtualgl virtualglLib vulkan-headers vulkan-tools
      ### DECODERS & CODECS
      ffmpeg ffmpegthumbnailer nv-codec-headers
      flac gst_all_1.gstreamer gst_all_1.gst-plugins-base gst_all_1.gst-plugins-viperfx gst_all_1.gst-vaapi gst_all_1.gst-libav gst_all_1.gst-plugins-good gst_all_1.gst-plugins-ugly gst_all_1.gst-plugins-bad
    ]
  
    # ADD CUSTOM USER CREATED SHELL PACKAGES
    ++ (lib.attrValues config.customOPTS.programs.shell.finalPackages)

    # DESKTOP-ONLY PKGS
    ++ lib.optionals (machineType != "server") [
      ### X11
      xclip
      xorg.libX11
      xorg.libX11.dev
      xorg.libxcb
      xorg.libXft
      xorg.libXinerama
      xorg.xhost
      xorg.xinit
      xorg.xinput

      ### SOFTWARE MGMT
      #polkit_gnome
      #lxqt.lxqt-policykit
      dconf
      desktop-file-utils  sound-theme-freedesktop
      gparted
      pavucontrol  pulsemixer
      appimage-run
      xdg-user-dirs xdg-utils
    ]
    
    ### SERVER-ONLY PKGS
    ++ lib.optionals (machineType == "server") [
    ];
  };
}
