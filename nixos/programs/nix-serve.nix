{
  config,
  inputs,
  lib,
  pkgs,
  ...
}: lib.mkIf (config.customOPTS.programs.nix-serve.enable) {
  services.nix-serve = {
    enable = true;
    port = 5000; # DEFAULT IS 5000
    openFirewall = true;
    secretKeyFile = "${inputs.penv}/config/system/nix-serve/secret";
    #extraParams = "";
    package = pkgs.nix-serve;
  };
  environment.systemPackages = [ pkgs.nix-serve ];
}
