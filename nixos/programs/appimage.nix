{
  config,
  pkgs,
  ...
}: {
  programs.appimage = { # APPIMAGE INTEGRATION: https://nixos.wiki/wiki/Appimage
    enable = true;
    binfmt = true;
    package = pkgs.appimage-run.override {extraPkgs = pkgs: [ pkgs.ffmpeg pkgs.imagemagick ];};
  };
}
