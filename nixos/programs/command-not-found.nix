{ # REF: https://youtu.be/sLGoWAGklds
  config,
  inputs,
  lib,
  pkgs,
  ...
}: lib.mkIf (config.customOPTS.programs.command-not-found.enable && !config.customOPTS.programs.nix-index.enable) {
  # environment.variables = {
  #   NIX_AUTO_RUN = "1"; # IF ONLY 1 PACKAGE FOUND RELATED TO THE PACKAGE WRITTEN, RUN IT AUTOMATICALLY WITHOUT INTERACTIVE PROMPT
  #   NIX_AUTO_INSTALL = "1";
  # };

  programs.command-not-found = {
      enable = true;
      dbPath = inputs.nixpkgs + "/programs.sqlite";
      #dbPath = "/nix/var/nix/profiles/per-user/root/channels/nixos/programs.sqlite";
    };
}
