{
  config,
  lib,
  pkgs,
  ...
}:
lib.mkIf (config.customOPTS.programs.docker.enable || config.customOPTS.programs.distrobox.enable) {
  virtualisation = {
    docker = {
      enable = true;
      liveRestore = false; # ENABLE dockerd SERVICE TO RESTART WITHOUT AFFECTING THE RUNNING CONTATINERS
      enableOnBoot = true;
      autoPrune.enable = true;
      storageDriver = if (lib.strings.hasInfix "btrfs" config.customOPTS.filesystems.osFS.format) then "btrfs" else null;
    };
    /*
    podman = {
      enable = true;
      # create a `docker` alias for podman, to use it as a drop-in replacement
      dockerCompat = true;
      # required for containers under podman-compose to be able to talk to each other.
      defaultNetwork.settings.dns_enabled = true;
    };
    containers.storage.settings = lib.mkIf (config.fileSystems."/".fsType == "zfs") {
      storage = {
        driver = "zfs";
        graphroot = "/var/lib/containers/storage";
        runroot = "/run/containers/storage";
      };
    };
    */
  };

  environment.systemPackages = with pkgs; [
    docker docker-buildx docker-compose docker-compose-language-service
  ] ++ lib.optional (config.customOPTS.programs.distrobox.enable) distrobox;
}
