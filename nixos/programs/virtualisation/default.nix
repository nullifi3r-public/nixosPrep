{config, ...}: {
  imports = [
    ./docker.nix
    ./virtmanager.nix
    ./virtualbox.nix
    ./vmware.nix
  ];
}
