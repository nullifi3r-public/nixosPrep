{ config, pkgs, lib, ... }:

lib.mkIf (config.customOPTS.programs.vmware.enable) {
  virtualisation.vmware.host = {
    enable = true;
    #package = pkgs.vmware-workstation.override { enableMacOSGuests = true; };
  };
  
  #boot.blacklistedKernelModules = [
  #  "kvm"
  #  "kvm_amd"
  #  "kvm_intel"
  #];
}