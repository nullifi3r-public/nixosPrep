{ config, pkgs, lib, ... }:

lib.mkIf (config.customOPTS.programs.virtmanager.enable) {
  # https://discourse.nixos.org/t/virt-manager-cannot-find-virtiofsd/26752/2
  # add virtiofsd to filesystem xml
  # <binary path="/run/current-system/sw/bin/virtiofsd"/>
  # Install necessary packages
  environment.systemPackages = with pkgs; [
    dosfstools
    libvirt
    lxc
    qemu
    spice spice-gtk
    spice-protocol
    swtpm
    uefi-run
    virt-manager
    virt-viewer
    virtiofsd
    win-spice
    win-virtio
  ];

  programs.virt-manager.enable = true;

  # Manage the virtualisation services
  virtualisation = {
    libvirtd = {
      enable = true;
      allowedBridges = [
        "nm-bridge"
        "virbr0"
        "virbr1"
        "virbr2"
        "virbr3"
        "virbr4"
        "virbr5"
      ];
      qemu = {
        runAsRoot = false;
        ovmf.enable = true;
        swtpm.enable = true;
        ovmf.packages = [ pkgs.OVMFFull.fd ];
      };
    };
    spiceUSBRedirection.enable = true;
  };

  customOPTS.filesystems.persist.root.directories = ["/var/lib/libvirt"];
}
