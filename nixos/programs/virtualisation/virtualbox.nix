{ config, pkgs, lib, ... }:

lib.mkIf (config.customOPTS.programs.virtualbox.enable) {
  environment.systemPackages = [pkgs.virtualbox];

  # Manage the virtualisation services
  virtualisation.virtualbox.host = {
    enable = true;
    enableWebService = false;
    addNetworkInterface = true;
    enableExtensionPack = true;
  };

  # ENABLE IF NON-STABLE KERNEL IS USED
  boot.extraModulePackages = with config.boot.kernelPackages; [ virtualbox ];
}
