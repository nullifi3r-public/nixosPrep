{
  config,
  pkgs,
  ...
}: {
  fonts = {
    fontDir.enable = true;
    fontconfig.includeUserConf = true;
    inherit (config.customOPTS.theming.fonts) packages;
  };

  customOPTS.filesystems.persist = {
    root.directories = ["/usr/share/fonts" "/usr/share/consolefonts" "/usr/share/fontconfig" "/usr/share/local/fonts"];
    root_cache.directories = ["/var/cache/fontconfig"];
  };
}
