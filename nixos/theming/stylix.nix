## YOU CAN USE A SPECIFIC COLOR WHEN NEEDED. E.G.:
#wayland.windowManager.hyprland.settings.general."col.active_border" = lib.mkForce "rgb(${config.stylix.base16Scheme.base0E})";
{
  config,
  inputs,
  lib,
  machineType,
  pkgs,
  ...
}: {
  imports = [inputs.stylix.nixosModules.stylix];

  stylix = lib.mkIf (config.customOPTS.theming.stylix.enable && machineType != "server") (config.customOPTS.theming.stylix.commonStylixConfig // {

    # WHETHER TO ENABLE TARGETS BY DEFAULT. WHEN THIS IS FALSE, ALL TARGETS ARE DISABLED UNLESS EXPLICITLY ENABLED
    autoEnable = false;

    #base16Scheme = "${pkgs.base16-schemes}/share/themes/windows-10.yaml";

 
    homeManagerIntegration = {
      autoImport = false;
      followSystem = false;
    };

    targets = {
      #gnome.enable = true;         # GNOME & GDM
      #gtk.enable = true;           # MANAGED MANULLY
      #console.enable = true;       # MANAGED MANULLY
      #kmscon.enable = true;        # MANAGED MANULLY
      #nixos-icons.enable = true;   # THE NIXOS LOGO
    };
  });
}
