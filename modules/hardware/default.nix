{ ... }:
{
  imports = [
    ./bluetooth.nix
    ./hwaccl.nix
    ./keyd.nix
    ./printing_scanning.nix
  ];
}
