{ lib, ... }:
{
  options.customOPTS.hw_services = {
    hwaccl.enable = lib.mkEnableOption "Enable Hardware Acceleration?" // {
      default = true;
    };
    hwaccl.vendor = lib.mkOption {
      type = with lib.types; nullOr str;
      default = null; # intel || amd. FOR NVIDEA, CREATE A DEDICATED NIX FILE
      description = "Apply HW Acceleration based on CPU Vendor";
    };
  };
}
