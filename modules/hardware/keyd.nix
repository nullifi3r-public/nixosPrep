{ lib, ... }:
{
  options.customOPTS.hw_services = {
    keyd.enable = lib.mkEnableOption "Enable Keyd Service?" // {
      default = false;
    };
  };
}
