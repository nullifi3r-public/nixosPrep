{ lib, isLaptop, ... }:
{
  options.customOPTS.hw_services = {
    bluetooth.enable = lib.mkEnableOption "Enable Bluetooth Support?" // {
      default = isLaptop;
    };
  };
}
