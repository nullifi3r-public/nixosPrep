{ lib, ... }:
{
  options.customOPTS.hw_services = {
    printing_scanning.enable = lib.mkEnableOption "Enable Printing and Scanning Support?" // {
      default = false;
    };
  };
}
