{ lib, ... }:
{
  options.customOPTS.desktops = {
    plasma.enable = lib.mkEnableOption "Install  Plasma?" // {
      default = false;
    };
  };
}
