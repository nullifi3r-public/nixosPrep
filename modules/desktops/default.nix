{ ... }:
{
  imports = [
    ./display_manager.nix
    ./hyprland.nix
    ./plasma.nix
  ];
}
