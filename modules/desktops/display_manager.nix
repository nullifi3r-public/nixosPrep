{ lib, ... }:
{
  options.customOPTS.desktops = {
    display_manager = lib.mkOption {
      type = with lib.types; nullOr str;
      default = null; # desktop, server, vm
      description = "The Display Manager to Use";
    };
  };
}
