{ lib, ... }:
{
  options.customOPTS.desktops = {
    hyprland.enable = lib.mkEnableOption "Install Hyprland?" // {
      default = false;
    };
  };
}
