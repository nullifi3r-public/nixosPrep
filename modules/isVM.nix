{ lib, ... }:
{
  options.customOPTS.isVM = {
    enable = lib.mkEnableOption "VM Support" // { default = false; };
    mounts.enable = lib.mkEnableOption "VM Virtual Mountpoints Support" // { default = false; };
    hypervisor = lib.mkOption {
      type = with lib.types; nullOr str;
      default = null; # virtmanager, virtualbox, vmware
      description = "The Hypervisor Platform";
    };
  };
}
