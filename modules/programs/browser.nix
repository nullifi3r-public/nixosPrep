{ lib, ... }:
{
  options.customOPTS.programs.browser = lib.mkOption {
    type = with lib.types; nullOr str;
    default = null; # vivaldi, firefox, brave, ..., etc
    description = "The Browser to Install";
  };
}
