{lib, ...}: {
  options.customOPTS.programs = {
    nh.enable = lib.mkEnableOption "Install nh?" // {
      default = false;
    };
  };
}
