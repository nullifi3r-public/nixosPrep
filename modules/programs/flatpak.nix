{ lib, ... }:
{
  options.customOPTS.programs = {
    flatpak.enable = lib.mkEnableOption "Enable Flatpak Support?" // {
      default = false;
    };
  };
}
