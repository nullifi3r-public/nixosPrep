{ lib, ... }:
{
  options.customOPTS.programs = {
    nix-index.enable = lib.mkEnableOption "Install nix-index?" // {
      default = false;
    };
  };
}
