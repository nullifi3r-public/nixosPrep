{
  config,
  inputs,
  lib,
  user,
  ...
}:
{
  options.customOPTS.programs = {
    sops.enable = lib.mkEnableOption "Enable SOPS Support?" // {
      default = false;
    };
  };
  
  config = lib.mkIf (config.customOPTS.programs.sops.enable) {
    sops = {
      validateSopsFiles = false;
      defaultSopsFile = "${inputs.sopsVault}/.secrets.yaml";
      age = let 
        key_location = if (config.customOPTS.filesystems.osFS.format == "btrfs-impermanence")
          then "/ps_home/home/${user}/.config/sops/age/nix.txt"
          else "/home/${user}/.config/sops/age/nix.txt";
      in {generateKey = false; keyFile = "${key_location}";};
      #defaultSymlinkPath = "/run/user/1000/secrets";
      #defaultSecretsMountPoint = "/run/user/$1000/secrets.d";
    };
  };
}
