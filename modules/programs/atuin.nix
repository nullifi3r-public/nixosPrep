{ lib, ... }:
{
  options.customOPTS.programs = {
    atuin.enable = lib.mkEnableOption "Install atuin?" // {
      default = false;
    };
  };
}
