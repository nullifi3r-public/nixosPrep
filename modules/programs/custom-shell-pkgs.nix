{
  config,
  lib,
  pkgs,
  ...
}: {
/* IDEA OF THIS MODULE:
  - The below modules will contain attribute sets where each attribute will represent a package/string (that will be converted to a package with `pkgs.writeShellApplication`)
  - To Define a package, write it inside `customOPTS.programs.shell.packages = {};`
  - AN OVERLAY WILL BE CREATED TO INCLUDE ALL GENERATED PACKAGES IN `finalPackages` UNDER pkgs.custom.shell
  - Finally, THE PACKAGES WILL BE INSTALLED USING THE  `++ (lib.attrValues config.customOPTS.programs.shell.finalPackages)` ADDED TO EACH _common.nix file (SEE BELOW "NOTE-1")

  TIP: USE https://noogle.dev to read about the functions used in this file

  NOTE-1: THERE'RE 2 OVERLAYS DEFINED,
    ONE IN nixos/programs/_common.nix
    THE OTHER IS IN home-manager/programes/_common.nix (ONLY ACTIVE IF OS IN NOT NixOS)
  NOTE-2: nixos config can access home-manager defined packages for the below modules using: config.hm.customOPTS.programs.shell.finalPackages (SEE THE OVERLAY DEFINED in nixos/programs/_common.nix)
*/

  options.customOPTS.programs.shell = {
    packages = lib.mkOption {
      type = with lib.types; attrsOf (oneOf [str attrs package]);
      default = { };
      description = ''
        Attrset of shell packages to install and add to pkgs.custom overlay (for compatibility across multiple shells).
        Both string and attr values will be passed as arguments to writeShellApplication
      '';
      example = ''
        shell.packages = {
          myPackage1 = "echo 'Hello, World!'";
          myPackage2 = {
            runtimeInputs = [ pkgs.hello ];
            text = "hello --greeting 'Hi'";
          };
        }
      '';
    };

    finalPackages = lib.mkOption {
      type = with lib.types; attrsOf package;
      readOnly = true;
      default = lib.mapAttrs (
        name: value:
          if lib.isString value then
            pkgs.writeShellApplication {
              inherit name;
              text = value;
            }
          # PACKAGE
          else if lib.isDerivation value then
            value
          # ATTRS TO PASS TO WRITESHELLAPPLICATION
          else
            pkgs.writeShellApplication (value // { inherit name; })
      ) config.customOPTS.programs.shell.packages;
      description = "Extra shell packages to install after all entries have been converted to packages.";
    };
  };
}
