{...}:{
  imports = [
    ./gaming

    ./atuin.nix
    ./browser.nix
    ./command-not-found.nix
    ./custom-shell-pkgs.nix
    ./docker.nix
    ./flameshot.nix
    ./flatpak.nix
    ./hypervisor.nix
    ./hypridle.nix
    ./hyprlock.nix
    ./nix-index.nix
    ./nix-serve.nix
    ./nomachine.nix
    ./sops.nix
    ./swaync.nix
    ./waybar.nix
  ];
}
