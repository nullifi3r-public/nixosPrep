{ config, lib, isLaptop, ... }:
{
  options.customOPTS.programs = {
    distrobox.enable = lib.mkEnableOption "Install Distrobox?" // {
      default = false;
    };
    docker.enable = lib.mkEnableOption "Install Docker?" // {
      default = config.customOPTS.programs.distrobox.enable;
    };
  };
}
