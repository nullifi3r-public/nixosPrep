{ lib, ... }:
{
  options.customOPTS.programs.gaming = {
    steam.enable = lib.mkEnableOption "Install Steam?" // {
      default = false;
    };
  };
}
