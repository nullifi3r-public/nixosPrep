{ lib, ... }:
{
  options.customOPTS.programs.gaming = {
    protonup-ng.enable = lib.mkEnableOption "Install Protonup-ng?" // {
      default = false;
    };
  };
}
