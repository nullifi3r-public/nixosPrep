{ lib, ... }:
{
  options.customOPTS.programs.gaming = {
    lutris.enable = lib.mkEnableOption "Install Lutris?" // {
      default = false;
    };
  };
}
