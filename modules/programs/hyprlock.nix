{ lib, ... }:
{
  options.customOPTS.programs = {
    hyprlock.enable = lib.mkEnableOption "Install Hyprlock?" // {
      default = false;
    };
  };
}
