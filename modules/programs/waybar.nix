{ lib, ... }:
{
  options.customOPTS.programs = {
    waybar.enable = lib.mkEnableOption "Install Waybar?" // {
      default = false;
    };
  };
}
