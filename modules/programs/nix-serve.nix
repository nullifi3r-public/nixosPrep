{ lib, ... }:
{
  options.customOPTS.programs = {
    nix-serve.enable = lib.mkEnableOption "Enable Nix-Serve Support?" // {
      default = false;
    };
  };
}
