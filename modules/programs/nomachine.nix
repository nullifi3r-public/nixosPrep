{lib, ...}: {
  options.customOPTS.programs = {
    nomachine.enable = lib.mkEnableOption "Install NoMachine RDM Solution (Server & Client)?" // {
      default = false;
    };
  };
}
