{ lib, ... }:
{
  options.customOPTS.programs = {
    swaync.enable = lib.mkEnableOption "Install Sway Notification Center?" // {
      default = false;
    };
  };
}
