{ lib, ... }:
{
  options.customOPTS.programs = {
    hypridle.enable = lib.mkEnableOption "Install Hypridle?" // {
      default = false;
    };
  };
}
