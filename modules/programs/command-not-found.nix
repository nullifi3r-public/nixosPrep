{ lib, ... }:
{
  options.customOPTS.programs = {
    command-not-found.enable = lib.mkEnableOption "Install Command-Not-Found?" // {
      default = false;
    };
  };
}
