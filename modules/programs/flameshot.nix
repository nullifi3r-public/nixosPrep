{ lib, ... }:
{
  options.customOPTS.programs = {
    flameshot.enable = lib.mkEnableOption "Install Flameshot?" // {
      default = false;
    };
  };
}
