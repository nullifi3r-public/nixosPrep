{ lib, ... }:
{
  options.customOPTS.programs = {
    virtmanager.enable = lib.mkEnableOption "Install Virtmanager?" // {
      default = false;
    };

    virtualbox.enable = lib.mkEnableOption "Install VirtualBox?" // {
      default = false;
    };
    
    vmware.enable = lib.mkEnableOption "Install Vmware-Workstation?" // {
      default = false;
    };
  };
}
