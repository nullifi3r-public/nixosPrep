{ # ADD NEW OPTIONS HERE TO REFER TO THIS FILE WHEN ADDING NEW HOSTS

  config.customOPTS = {
    isVM = {
      enable = true;
      mounts.enable = true;
      hypervisor = "virtmanager";
    };


    desktops = {
      display_manager = "greetd";
      hyprland.enable = false;
      plasma.enable = false;
    };

    filesystems = {
      osFS.format = "btrfs-impermanence";
      network = {
        # TASK: TEST glusterfs
        fileServers.nfs.enable = false;
        fileServers.smb.enable = false;
        mounts.enable = false; # NEVER AUTO-MOUNT NFS, CAUSES HEADACHE IF NOT REACHABLE OVER NETWORK
      };
    };

    hw_services = { # BLUETOOTH SERVICE IS MANAGED BY THE isLaptop VARIABLE
      hwaccl = {
        enable = true;
        vendor = "intel";
      };
      keyd.enable = false;
      printing_scanning.enable = false;
    };

    programs = {
      atuin.enable              = false;
      command-not-found.enable  = false;
      flameshot.enable          = false;
      nix-index.enable          = false;
      sops.enable               = false;

      browser                   = "vivaldi";
      distrobox.enable          = false;
      docker.enable             = false;
      flatpak.enable            = false;
      nix-serve.enable          = false;

      virtmanager.enable        = false;
      vmware.enable             = false;
      virtualbox.enable         = false;

      hypridle.enable           = false;
      hyprlock.enable           = false;
      swaync.enable             = false;
      waybar.enable             = false;

      gaming                    = {
        lutris.enable             = false;
        protonup-ng.enable        = false;
        steam.enable              = false;
      };
    };

    theming.stylix.enable = true;
  };
}
