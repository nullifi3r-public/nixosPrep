{ lib, ... }:
{
  options.customOPTS.filesystems.persist = {
    root = {
      directories = lib.mkOption {
        type = with lib.types; listOf str;
        default = [ ];
        description = "Directories to persist in root filesystem";
      };
      files = lib.mkOption {
        type = with lib.types; listOf str;
        default = [ ];
        description = "Files to persist in root filesystem";
      };
    };
    root_cache = {
      directories = lib.mkOption {
        type = with lib.types; listOf str;
        default = [ ];
        description = "Directories to persist in root, but not to snapshot";
      };
      files = lib.mkOption {
        type = with lib.types; listOf str;
        default = [ ];
        description = "Files to persist in root, but not to snapshot";
      };
    };

    home = {
      directories = lib.mkOption {
        type = with lib.types; listOf str;
        default = [ ];
        description = "Directories to persist in home directory";
      };
      files = lib.mkOption {
        type = with lib.types; listOf str;
        default = [ ];
        description = "Files to persist in home directory";
      };
    };
    home_cache = {
      directories = lib.mkOption {
        type = with lib.types; listOf str;
        default = [ ];
        description = "Directories to persist in home, but not to snapshot";
      };
      files = lib.mkOption {
        type = with lib.types; listOf str;
        default = [ ];
        description = "Files to persist in home, but not to snapshot";
      };
    };
  };
}
