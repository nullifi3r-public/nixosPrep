{...}:{
  imports = [
    ./filesystem_properties.nix
    ./impermanence.nix
    ./network_filesystems.nix
  ];
}
