{lib, ...}: {
  options.customOPTS.filesystems.osFS = {
    format = lib.mkOption {
      type = with lib.types; nullOr str;
      default = null; # CAN BE: btrfs, btrfs-impermanence, zfs, zfs-impermanence, ...
      description = "TYPE OF ROOT'S FILESYSTEM";
    };
  };
}
