{ lib, ... }:
{
  options.customOPTS.filesystems.network = {
    fileServers.nfs.enable = lib.mkEnableOption "Enable NFS Network Server?" // {  # CHECK THIS: https://github.com/etu/nixconfig/blob/main/modules/services/nfs/default.nix
      default = false;
    };
    fileServers.smb.enable = lib.mkEnableOption "Enable SAMBA Network Server?" // {
      default = false;
    };
    mounts.enable = lib.mkEnableOption "Enable NFS Network Mounts?" // {
      default = false;
    };
  };
}
