{ ... }:
{
  imports = [
    ./desktops
    ./filesystems
    ./hardware
    ./programs
    ./theming

    ./isVM.nix
  ];
}
