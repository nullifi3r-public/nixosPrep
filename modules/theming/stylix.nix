{
  config,
  inputs,
  lib,
  pkgs,
  ...
}: {
  options.customOPTS.theming.stylix = {
    enable = lib.mkEnableOption "Enable Stylix Theming?" // {
      default = false;
    };

    commonStylixConfig = lib.mkOption {
      description = "Attribute set of options common between HM & NixOS modules";
      type = lib.types.attrs;
      default = {
        ## GLOBAL SWITCH
        enable = true;

        polarity = "dark";
        ## GENERATE FROM A WALLPAPER (PATH OR FETCH ONLINE)
        #image = ./path/to/image.png
        image = pkgs.fetchurl {
          url = "https://raw.githubusercontent.com/NixOS/nixos-artwork/f07707cecfd89bc1459d5dad76a3a4c5315efba1/wallpapers/nix-wallpaper-nineish-dark-gray.png";
          sha256 = "9e1214b42cbf1dbf146eec5778bde5dc531abac8d0ae78d3562d41dc690bf41f";
        };
        
        ## BASE16-SCHEME (OPTIONAL | WHEN NOT SET SCHEME IS AUTO-GENERATED FROM IMAGE)
        base16Scheme = "${inputs.penv}/config/user/dblend-base16.yaml";
        #base16Scheme = "${pkgs.base16-schemes}/share/themes/ayu-dark.yaml";

        # base16Scheme = { # SAME COLORS IN THE YAML FILE
        #   base00 = "121314";
        #   base01 = "222426";
        #   base02 = "2E3E42";
        #   base03 = "666D78";
        #   base04 = "D58742";
        #   base05 = "FFFFFF";
        #   base06 = "D19A66";
        #   base07 = "FFFFFF";
        #   base08 = "E06C75";
        #   base09 = "D19A66";
        #   base0A = "FFB454";
        #   base0B = "B8CC52";
        #   base0C = "FF8F40";
        #   base0D = "6095D5";
        #   base0E = "D1AEDC";
        #   base0F = "3778A6";
        # };

        ## CURSOR OPTIONS
        cursor = {
          name = "Bibata-Modern-Ice";     # "Nordzy-cursors";
          package = pkgs.bibata-cursors;  # pkgs.nordzy-cursor-theme;
          size = 24;
        };
        ## FONTS OPTIONS
        fonts = {
          sizes = {
            applications = 12;
            terminal = 9;
            desktop = 10;
            popups = 10;
          };
          monospace = {
            package = pkgs.nerdfonts.override {fonts = ["JetBrainsMono"];};
            name = "JetBrainsMono Nerd Font Mono";
          };
          sansSerif = {
            package = inputs.apple-fonts.packages.${pkgs.system}.sf-pro;
            name = "SF Pro Display";
          };
          serif = {
            package = inputs.apple-fonts.packages.${pkgs.system}.sf-pro;
            name = "SF Pro Display";
          };
          emoji = {
            package = pkgs.twitter-color-emoji;
            name = "Twitter Color Emoji";
          };
        };
        ## OPACITY OPTIONS
        opacity = {
          applications = 1.0;
          terminal = 0.9;
          desktop = 1.0;
          popups = 1.0;
        };
      };
    };
  };
}
