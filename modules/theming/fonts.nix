{
  inputs,
  lib,
  machineType,
  pkgs,
  ...
}: {
  options.customOPTS.theming = {
    fonts.packages = lib.mkOption {
      type = with lib.types; listOf package;
      default = let
        apple-fonts = inputs.apple-fonts.packages.${pkgs.system};
      in [
        ## EMOJI FONTS
        pkgs.noto-fonts-color-emoji  pkgs.twitter-color-emoji
        ## SYMPOLIC FONTS
        pkgs.emacs-all-the-icons-fonts pkgs.font-awesome_5 pkgs.material-icons pkgs.symbola
        ## APPPLE FONTS
        apple-fonts.sf-arabic # I DON'T NEED AN ARABIC NERDFONT
        ## MICROSOFT FONTS
        pkgs.corefonts pkgs.vistafonts
        ## GENERIC FONTS
        pkgs.dejavu_fonts pkgs.freefont_ttf pkgs.liberation_ttf pkgs.noto-fonts pkgs.unifont
        pkgs.gyre-fonts # TrueType substitutes for standard PostScript fonts
        ## NERDFONTS
        (pkgs.nerdfonts.override { fonts = ["JetBrainsMono" "Ubuntu"]; })
        #(pkgs.nerdfonts.override { fonts = ["DroidSansMono" "FiraCode" "Hermit"  "JetBrainsMono" "ProggyClean" "Meslo" "Ubuntu"]; })
      ] ++ 
        (if (machineType == "desktop")
          ## APPPLE FONTS
          then [
            #apple-fonts.ny-nerd
            apple-fonts.sf-pro-nerd
          ]
          else [
            #apple-fonts.ny
            apple-fonts.sf-pro
          ]);
      description = "Fonts Packages To Install";
    };
  };
}
