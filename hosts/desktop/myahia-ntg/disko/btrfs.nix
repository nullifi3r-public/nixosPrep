{user ? "yahia", ...}: {
  fileSystems."/".neededForBoot = true;
  fileSystems."/nix".neededForBoot = true;

  disko.devices = {
    disk.main = {
      type = "disk";
      device = "/dev/nvme0n1"; ### DOUBLE CHECK BEFORE EXECUTING DISKO
      content = {
        type = "gpt";
        partitions = {
          boot = {
            name = "NIXBOOT";
            label = "NIXBOOT";
            size = "80M";
            type = "EF02";
          };
          esp = {
            name = "NIXEFI";
            label = "NIXEFI";
            size = "6000M";
            type = "EF00";
            content = {
              type = "filesystem";
              format = "vfat";
              mountpoint = "/boot";
            };
          };
          root = {
            name = "NIXROOT";
            label = "NIXROOT";
            end = "-16G";  ### CHANGE ACCORDING TO THE RAM AVAILABLE TO ACCOMMODATE SWAP SIZE
            content = {
              type = "btrfs";
              extraArgs = ["-f"];
              subvolumes = {
                "/@root" = {
                  mountpoint = "/";
                  mountOptions = ["subvol=@root" "compress=zstd" "noatime"];
                };
                "/@root/.snapshots" = {
                  mountpoint = "/.snapshots";
                  mountOptions = ["subvol=@root/.snapshots" "compress=zstd" "noatime"];
                };
                "/@home" = {
                  mountpoint = "/home";
                  mountOptions = ["subvol=@home" "compress=zstd" "noatime"];
                };
                "/@home/.snapshots" = {
                  mountpoint = "/home/.snapshots";
                  mountOptions = ["subvol=@home/.snapshots""compress=zstd" "noatime"];
                };
                "/@storageHub" = {
                  mountpoint = "/home/${user}/storageHub";
                  mountOptions = ["subvol=@storageHub" "compress=zstd" "noatime"];
                };
                "/@storageHub/.snapshots" = {
                  mountpoint = "/home/${user}/storageHub/.snapshots";
                  mountOptions = ["subvol=@storageHub/.snapshots" "compress=zstd" "noatime"];
                };
                "/@nix" = {
                  mountpoint = "/nix";
                  mountOptions = ["subvol=@nix" "compress=zstd" "noatime"];
                };
                "@tmp" = {
                  mountpoint = "/tmp";
                  mountOptions = ["subvol=@tmp" "compress=zstd" "noatime"];
                };
                "/var" = {
                  mountpoint = "/var";
                  mountOptions = ["compress=zstd" "noatime"];
                };
                "/@var/log" = {
                  mountpoint = "/var/log";
                  mountOptions = ["subvol=@var/logs" "compress=zstd" "noatime"];
                };
                "/@var/tmp" = {
                  mountpoint = "/var/tmp";
                  mountOptions = ["subvol=@var/tmp" "compress=zstd" "noatime"];
                };
                "/@docker" = {
                  mountpoint = "/var/lib/docker";
                  mountOptions = ["subvol=@docker" "compress=zstd" "noatime"];
                };
                "/@podman" = {
                  mountpoint = "/var/lib/containers";
                  mountOptions = ["subvol=@podman" "compress=zstd" "noatime"];
                };
              };
              mountpoint = "/btrfs-root";
            };
          };
          swap = {
            name = "SWAP";
            label = "SWAP";
            size = "100%";
            content = {
              type = "swap";
              resumeDevice = true;
            };
          };
        };
      };
    };
  };
}
