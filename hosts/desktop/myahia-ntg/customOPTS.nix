{
  config,
  ...
}: {
  config.customOPTS = {
    filesystems.osFS.format = "btrfs-impermanence";
    theming.stylix.enable = true;

    desktops = {
      display_manager = "greetd";
      hyprland.enable = true;
    };

    hw_services = {
      hwaccl = {
        enable = true;
        vendor = "intel";
      };
      keyd.enable = true;
    };

    programs = {
      atuin.enable              = true;
      browser                   = "vivaldi";
      distrobox.enable          = true;
      flatpak.enable            = true;
      nix-index.enable          = true;
      nomachine.enable          = true;
      sops.enable               = true;

      virtmanager.enable        = true;
      vmware.enable             = true;

      hypridle.enable           = true;
      hyprlock.enable           = true;
      swaync.enable             = true;
      waybar.enable             = true;
    };
  };
}
