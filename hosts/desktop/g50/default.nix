{...}: {
  imports = [
    ./disko/btrfs-impermanence.nix
    ./hardware.nix
    ./customOPTS.nix
  ];
}
