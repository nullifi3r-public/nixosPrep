{
  inputs,
  lib,
  system,
  specialArgs,
  user ? "yahia",
  ...
}:
let
  # PROVIDE AN OPTIONAL { pkgs } 2ND ARGUMENT TO OVERRIDE THE PKGS
  mkNixosConfiguration =
    host:
    {
      pkgs ? (
        import inputs.nixpkgs {
          inherit system;
          config.allowUnfree = true;
        }
      ),
    }:let 
        machineType = if (lib.lists.elem host [
          "g50"
          "myahia-ntg"
          ]) then "desktop"
        else (if (lib.lists.elem host [
          "toshiba"
        ]) then "server"
        else (if (lib.strings.hasInfix "-vm" host) then "vm" else "custom"));
    in lib.nixosSystem  {
      inherit pkgs;
      specialArgs = specialArgs // {
        inherit host user machineType;
        isNixOS = true;

        ########################
        ## host-BASED VARIABLES
        ########################
        isLaptop = if (lib.lists.elem host [
          "g50"
          "myahia-ntg"
          "toshiba"
        ]) then true else false;

        storageHub_state = if (lib.lists.elem host [
          "g50"
          ]) then "external"
        else ( if (lib.lists.elem host [
          "xyz"
          ]) then "n/a"
          else "sameDiskOfRoot"
        );

        bootloader = if ((lib.strings.hasInfix "-vm" host) || (lib.lists.elem host [
          "toshiba"
          ])) then "grub-bios"
        else ( if (lib.lists.elem host [
          "xyz"
          ]) then "systemd"
          else "grub-efi"
        );
      };

      modules = [
        inputs.disko.nixosModules.default
        inputs.home-manager.nixosModules.home-manager
        inputs.impermanence.nixosModules.impermanence
        inputs.sops-nix.nixosModules.sops
      ../overlays
      ../modules
      ../nixos
      ./${machineType}/${host}
        {
          home-manager = {
            sharedModules = [inputs.sops-nix.homeManagerModules.sops];
            useGlobalPkgs = true;
            useUserPackages = true;
            backupFileExtension = "hm.bak";
            verbose = true;

            extraSpecialArgs = specialArgs // {
              inherit host user machineType;
              isNixOS = true;
              isLaptop = if (lib.lists.elem host [
                "g50"
                "myahia-ntg"
                "toshiba"
              ]) then true else false;
            };

            users.${user} = {
              imports = [
                inputs.nix-flatpak.homeManagerModules.nix-flatpak
                inputs.stylix.homeManagerModules.stylix
                ../overlays
                ../modules
                ./${machineType}/${host}/customOPTS.nix
                ../home-manager
              ];
            };
          };
        }
        # alias for home-manager
        (lib.mkAliasOptionModule [ "hm" ] [
          "home-manager"
          "users"
          user
        ])
      ];
    };
in
{
  #cache-vm = mkNixosConfiguration "cache-vm" { };
  #hyprland-vm = mkNixosConfiguration "hyprland-vm" { };
  plasma-vm = mkNixosConfiguration "plasma-vm" { };
  #g50 = mkNixosConfiguration "g50" { };
  #myahia-ntg = mkNixosConfiguration "myahia-ntg" { };
  #toshiba = mkNixosConfiguration "toshiba" { };
}
