{
  config,
  ...
}: {
  config.customOPTS = {
    filesystems = {
      osFS.format = "btrfs-impermanence";
      network.fileServers.nfs.enable = true;
    };

    hw_services = { 
      bluetooth.enable = lib.mkForce false;
      keyd.enable = true;
    };

    programs = {
      atuin.enable              = true;
      sops.enable               = true;
      distrobox.enable          = true;
      nix-serve.enable          = true;
    };
  };
}
