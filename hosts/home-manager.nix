{
  inputs,
  lib,
  system,
  specialArgs,
  user ? "yahia",
  ...
}:
let
  # provide an optional { pkgs } 2nd argument to override the pkgs
  mkHomeConfiguration =
    host:
    {
      pkgs ? (
        import inputs.nixpkgs {
          inherit system;
          config.allowUnfree = true;
        }
      ),
    }: let 
        machineType = if (lib.lists.elem host [
          "g50"
          "myahia-ntg"
          ]) then "desktop"
        else (if (lib.lists.elem host [
          "toshiba"
        ]) then "server"
        else (if (lib.strings.hasInfix "-vm" host) then "vm" else "custom"));
    in inputs.home-manager.lib.homeManagerConfiguration {
      inherit pkgs;

      extraSpecialArgs = specialArgs // {
        inherit host user machineType;
        isNixOS = false;
        isLaptop = if (lib.lists.elem host [
          "g50"
          "myahia-ntg"
          "toshiba"
        ]) then true else false;

      };

      modules = [
        inputs.nix-flatpak.homeManagerModules.nix-flatpak
        inputs.sops-nix.homeManagerModules.sops
        inputs.stylix.homeManagerModules.stylix
        ../overlays
        ../modules
        ./${machineType}/${host}/customOPTS.nix
        ../home-manager
      ];
    };
in {
  #cache-vm = mkHomeConfiguration "cache-vm" { };
  #hyprland-vm = mkHomeConfiguration "hyprland-vm" { };
  plasma-vm = mkHomeConfiguration "plasma-vm" { };
  #g50 = mkHomeConfiguration "g50" { };
  #myahia-ntg = mkHomeConfiguration "myahia-ntg" { };
  #toshiba = mkHomeConfiguration "toshiba-homelab" { };
}
