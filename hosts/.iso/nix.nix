{...}:
let
  nixPath = ["nixpkgs=flake:nixpkgs" "/nix/var/nix/profiles/per-user/root/channels"];
in {
  nix = {
    inherit nixPath;
    channel.enable = false;
    settings = {
      nix-path = nixPath;
      trusted-users = ["root" "nixos"];
      experimental-features = ["nix-command" "flakes"];
      substituters = [
        "https://hyprland.cachix.org"
        "https://nix-community.cachix.org"
        "https://nix-gaming.cachix.org"
        "https://nixpkgs-wayland.cachix.org"
      ];
      trusted-public-keys = [
        "nix-cache.local-1:cOyMKqbEHdS58jshGxG49U7FdrXN9ijqeeR/7XNX0NY="
        "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        "nix-gaming.cachix.org-1:nbjlureqMbRAxR1gJ/f3hxemL9svXaZF/Ees8vCUUs4="
        "nixpkgs-wayland.cachix.org-1:3lwxaILxMRkVhehr5StQprHdEo4IrE8sRho9R9HOLYA="
      ];
    };
  };
}
