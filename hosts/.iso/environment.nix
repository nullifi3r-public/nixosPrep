{pkgs, ...}:
{
  environment = {
    ## PACKAGESA
    systemPackages = with pkgs; [bat bat-extras.batman btop curl disko expect git home-manager inetutils nixos-install-tools openssl_3_3 rsync tmux yazi];

    ## SESSION VARIABLES
    sessionVariables."SSH_AUTH_SOCK" = "/run/user/1000/ssh-agent";
    
    ## SHELL ALIASES
    shellAliases = {
      la = "ls -a";
      ll = "ls -l";
      lla = "ls -la";
      man = "batman";
      y = "yazi";
    };

    ## NEEDED FILES
    etc = {
      "encryptedSecrets/nixAgeKey.enc" = {
        uid = 0;
        gid = 0;
        mode = "0600";
        text = ''
          U2FsdGVkX19C9G+hg9sS8B0Cuk1jsKjQFglGMZoF6F4PJtyCPwLKY5uSGFC4YCro
          C3z/dNeyxDSYCLfoixnDfCXm7A5v2ORrzODgOvR74hYw7i60mq81HbwbgvPhSNJp
        '';
      };
      "encryptedSecrets/YUniKey.enc" = {
        uid = 0;
        gid = 0;
        mode = "0600";
        text = ''
          U2FsdGVkX1/yV5ueCmepJT5w0B2VAt5ij2cmcuc0a8uWRHH9PV/ZBfyACfG6qQB3
          +ouSuJeLQAW5lMq+xR0m8u/mdn1/5aYrqq7ymDTI8GRZxie6ETyHWesld9xGNrnF
          Mj4kZbImaulNv8KUnkQ4wAUA6av0QwEU1aFF8+rSu0J9mnEiQpqPz+/8xFIXJFPz
          W4MtEsCFbRjBesnfL1L5KHHDCw7hIHOmTGP6vx/POtqEJq6BaQybTHPd3WjB4YKI
          OmL4H4zvgNGmkvcdJWNAqHMx4J77/atcfQw5+SdTdYx3rQXJJwgE0YozJCeXBqho
          l8zjvfC+HqYpN/7lVuBtfLa55Q2QPKko79QWXpFbC6fQJaAU8v7USHE7Y5T6RCQz
          0sprhKZz7hbt0ELmg4kLkBxZWI+ykJZO/itBPoya/cZEv8nyrm6EK2i2gSOP+gx+
          VMrnmaoLO/1cF7JlpzWYoE2a2gxShx9H8GBliujorRta32K2HeVjqHgVzoyj7tzT
          Zfs7IbbDzl3ghnHeqKsidGceSQRTtCiuR7jmwhyF5iZIwFucxUkkIFu5HMy/ffNz
          yMXSO1DaHJLb3OoCDQiPjXcFySuQFaScRnzlzsjbDvg=
        '';
      };
    };
  };
}
