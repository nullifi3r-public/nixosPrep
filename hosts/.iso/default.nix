{
  inputs,
  lib,
  system ? "x86_64-linux",
  ...
}: let
  user = "nixos";
  mkIso = nixpkgs: isoPath: lib.nixosSystem {
    inherit system;
    modules = [
      #{ isoImage.squashfsCompression = "lz4"; }
      #{ isoImage.squashfsCompression = "gzip -Xcompression-level 1"; }
      { isoImage.squashfsCompression = "xz -Xdict-size 100%"; }
      "${nixpkgs}/nixos/modules/installer/cd-dvd/${isoPath}.nix"
      inputs.home-manager.nixosModules.home-manager
      ./nix.nix
      ./environment.nix 
      (import ./networking.nix {inherit inputs;})
      (import ./system.nix {inherit inputs lib user;})
      {
        home-manager = {
          useGlobalPkgs = true;
          useUserPackages = true;

          extraSpecialArgs = {
            inherit user isoPath inputs;
          };

          users.${user} = {
            imports = [ ./home.nix ];
          };
        };
      }
    ];
  };
in {
  minimal-iso-unstable = mkIso inputs.nixpkgs "installation-cd-minimal";
  #minimal-iso-stable = mkIso inputs.nixpkgs-stable "installation-cd-minimal";
  #kde-iso-unstable = mkIso inputs.nixpkgs "installation-cd-graphical-calamares-plasma6";
  #kde-iso-stable = mkIso inputs.nixpkgs-stable "installation-cd-graphical-calamares-plasma6";
}
# nix build .#nixosConfigurations.minimal-iso-unstable.config.system.build.isoImage
# qemu-system-x86_64 -enable-kvm -m 256 -cdrom result/iso/nixos-*.iso