{
  inputs,
  lib,
  user,
  ...
}:
{
  boot.loader.timeout = lib.mkForce 1;

  programs = {
    adb.enable = true;
    bash.completion.enable = true;
    nano.enable = false;
    neovim = {enable = true; viAlias = true; vimAlias = true; defaultEditor = true;};
    ssh = {
      startAgent = true;
      extraConfig = ''
        StrictHostKeyChecking no
      '';
    };
  };

  services = {
    openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = false;
        KbdInteractiveAuthentication = false;
      };
    };
    journald = {
      extraConfig = "SystemMaxUse=50M\nSystemMaxFiles=5";
      rateLimitBurst = 500;
      rateLimitInterval = "30s";
      console = "/dev/tty6";
    };
  };


  systemd = {
    oomd.enable = lib.mkForce false;
    services.sshd.wantedBy = lib.mkForce [ "multi-user.target" ];
  };
  users.users.root.openssh.authorizedKeys.keys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGNrlVEIsQUyHVnmP+a7h9W4U02yNBTZ/P4WTkig1lay YUniKey"];
  users.users.${user}.openssh.authorizedKeys.keys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGNrlVEIsQUyHVnmP+a7h9W4U02yNBTZ/P4WTkig1lay YUniKey"];
}
