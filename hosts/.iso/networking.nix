{inputs, ...}: let
  sb_hosts_gambling_porn = builtins.readFile "${inputs.stevenblack-hosts}/alternates/gambling-porn/hosts";
in {
  networking = {
    domain = "local";
    hostName = "nixos-iso";
    extraHosts = ''
      "${sb_hosts_gambling_porn}"
    '';
    firewall.enable = false;
  };
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    nssmdns6 = true;
    reflector = true;
    openFirewall = true; # ensuring that firewall ports are open as needed
    publish = {
      enable = true;
      addresses = true;
      workstation = true;
      userServices = true;
      domain = true;
    };
  };
}