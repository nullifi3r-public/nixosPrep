{...}: {
  imports = [
    ./hardware.nix
    ./customOPTS.nix
    ../_common
  ];
}
