{
  config,
  ...
}: {
  config.customOPTS = {
    filesystems.osFS.format = "btrfs-impermanence";
    theming.stylix.enable = false; # STYLIX IS SHITTY ON PLASMA

    isVM = {
      enable = true;
      hypervisor = "virtmanager";
      mounts.enable = true;
    };

    desktops = {
      display_manager = "sddm";
      plasma.enable = true;
    };

    hw_services = {
      hwaccl = {
        enable = true;
        vendor = "intel";
      };
      keyd.enable = true;
    };

    programs = {
      browser                   = "vivaldi";
      sops.enable               = true;
    };
  };
}
