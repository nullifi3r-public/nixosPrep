{
  config,
  ...
}: {
  config.customOPTS = {
    filesystems.osFS.format = "btrfs-impermanence";
    # theming.stylix.enable = true;

    isVM = {
      enable = true;
      hypervisor = "virtmanager";
      mounts.enable = true;
    };

    desktops = {
      display_manager = "greetd";
      hyprland.enable = true;
    };

    hw_services = {
      hwaccl = {
        enable = true;
        vendor = "intel";
      };
      keyd.enable = true;
    };

    programs = {
      browser                   = "vivaldi";
      sops.enable               = true;
    };
  };
}
