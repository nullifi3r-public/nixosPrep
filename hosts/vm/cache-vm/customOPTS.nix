{
  config,
  ...
}: {
  config.customOPTS = {
    filesystems.osFS.format = "btrfs-impermanence";

    isVM = {
      enable = true;
      hypervisor = "virtmanager";
      mounts.enable = true;
    };

    hw_services = {
      keyd.enable = true;
    };

    programs = {
      nix-serve.enable          = true;
      sops.enable               = true;
    };
  };
}
