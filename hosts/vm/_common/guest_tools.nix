{
  config,
  lib,
  pkgs,
  ...
}:
lib.mkMerge [
  (lib.mkIf (config.customOPTS.isVM.hypervisor == "vmware") {virtualisation.vmware.guest.enable = true;})

  (lib.mkIf (config.customOPTS.isVM.hypervisor == "virtualbox") {virtualisation.virtualbox.guest.enable = true;})

  (lib.mkIf (config.customOPTS.isVM.hypervisor == "virtmanager") {
    services = {
      qemuGuest.enable = true;
      spice-vdagentd.enable = true;
    };
    environment.systemPackages = with pkgs; [qemu_kvm.ga spice-vdagent];
  })
]
