{user ? "yahia", ...}: {
  fileSystems."/".neededForBoot = true;
  fileSystems."/nix".neededForBoot = true;
  fileSystems."/ps_cache".neededForBoot = true;
  fileSystems."/ps_home".neededForBoot = true;
  fileSystems."/ps_root".neededForBoot = true;

  disko.devices = {
    disk.main = {
      type = "disk";
      device = "/dev/vda"; ### DOUBLE CHECK BEFORE EXECUTING DISKO
      content = {
        type = "gpt";
        partitions = {
          boot = {
            name = "NIXBOOT";
            label = "NIXBOOT";
            size = "24M";
            type = "EF02";
          };
          esp = {
            name = "NIXEFI";
            label = "NIXEFI";
            size = "1000M";
            type = "EF00";
            content = {
              type = "filesystem";
              format = "vfat";
              mountpoint = "/boot";
            };
          };
          root = {
            name = "NIXROOT";
            label = "NIXROOT";
            size = "100%";
            content = {
              type = "lvm_pv";
              vg = "root_vg";
            };
          };
        };
      };
    };
    lvm_vg = {
      root_vg = {
        type = "lvm_vg";
        lvs = {
          root = {
            size = "100%FREE";
            content = {
              type = "btrfs";
              extraArgs = ["-f"];
              subvolumes = {
                "/@root" = {
                  mountpoint = "/";
                  mountOptions = ["subvol=@root" "compress=zstd" "noatime"];
                };
                "/@storageHub" = {
                  mountpoint = "/home/${user}/storageHub";
                  mountOptions = ["subvol=@storageHub" "compress=zstd" "noatime"];
                };
                "/@storageHub/.snapshots" = {
                  mountpoint = "/home/${user}/storageHub/.snapshots";
                  mountOptions = ["subvol=@storageHub/.snapshots" "compress=zstd" "noatime"];
                };
                "/@nix" = {
                  mountpoint = "/nix";
                  mountOptions = ["subvol=@nix" "compress=zstd" "noatime"];
                };
                "/@docker" = {
                  mountpoint = "/var/lib/docker";
                  mountOptions = ["subvol=@docker" "compress=zstd" "noatime"];
                };
                /*"/@podman" = {
                  mountpoint = "/var/lib/containers";
                  mountOptions = ["subvol=@podman" "compress=zstd" "noatime"];
                };*/
                "/@ps_cache" = {
                  mountpoint = "/ps_cache";
                  mountOptions = ["subvol=@ps_cache" "compress=zstd" "noatime"];
                };
                "/@ps_home" = {
                  mountpoint = "/ps_home";
                  mountOptions = ["subvol=@ps_home" "compress=zstd" "noatime"];
                };
                "/@ps_home/.snapshots" = {
                  mountpoint = "/ps_home/.snapshots";
                  mountOptions = ["subvol=@ps_home/.snapshots" "compress=zstd" "noatime"];
                };
                "/@ps_root" = {
                  mountpoint = "/ps_root";
                  mountOptions = ["subvol=@ps_root" "compress=zstd" "noatime"];
                };
                "/@ps_root/.snapshots" = {
                  mountpoint = "/ps_root/.snapshots";
                  mountOptions = ["subvol=@ps_root/.snapshots" "compress=zstd" "noatime"];
                };
              };
              mountpoint = "/btrfs-root";
            };
          };
        };
      };
    };
  };
}
