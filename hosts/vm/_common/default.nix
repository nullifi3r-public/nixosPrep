{...}: {
  imports = [
    ./disko/btrfs-impermanence.nix
    ./guest_tools.nix
    ./mounts.nix
  ];
}
