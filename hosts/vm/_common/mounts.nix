{  # users,exec,suid
  config,
  isNixOS,
  lib,
  pkgs,
  user,
  ...
}: let
  mount_source_device = if (config.customOPTS.isVM.hypervisor == "vmware")
    then ".host:/sharedArea"
    else "sharedArea";

  mount_filesystem = if (config.customOPTS.isVM.hypervisor == "virtmanager")
    then "virtiofs"
    else (if (config.customOPTS.isVM.hypervisor == "vmware")
      then "vmhgfs"
      else "vboxsf");
  
  mount_parent = "/home/${user}/storageHub";

  mount_path = lib.concatStrings ["${mount_parent}" "/.YOS"];

in lib.mkIf ((isNixOS) && (config.customOPTS.isVM.mounts.enable)) {

  fileSystems."${mount_path}" = {
    device = mount_source_device;

    fsType = mount_filesystem;

    options = [
      "_netdev"
      "nofail"
      "defaults"
      "users"
      "suid"
      "rw"
      "exec"
    ];
    depends = [
      "${mount_parent}"
    ];
  };
}
