{
  config,
  lib,
  machineType,
  pkgs,
  ...
}: lib.mkIf (machineType != "vm") {
  services.nextcloud-client = {
    enable = true;
    startInBackground = true;
    package = pkgs.nextcloud-client;
  };

  home.packages = [ pkgs.nextcloud-client ];
}
