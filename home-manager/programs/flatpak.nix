{
  config,
  isNixOS,
  lib,
  pkgs,
  user,
  ...
}: lib.mkIf (config.customOPTS.programs.flatpak.enable) {
  home = {
    sessionVariables.XDG_DATA_DIRS = "$XDG_DATA_DIRS:/var/lib/flatpak/exports/share:$HOME/.local/share/flatpak/exports/share";
    activation.RefreshInstalledFlatpaks = lib.hm.dag.entryAfter ["writeBoundary"] '' ${pkgs.flatpak}/bin/flatpak --user repair '';
  };

  services.flatpak = {
    enable = true;
    update = {
      onActivation = false; # NOT RECOMMENDED TO SET TO TRUE
      auto = {
        enable = true;
        onCalendar = "weekly"; # Default value
      };
    };

    ### By default nix-flatpak will add the flathub remote. Remotes can be manually configured via the services.flatpak.remotes option

    #remotes = [
    #  {name = "flathub-beta"; location = "https://flathub.org/beta-repo/flathub-beta.flatpakrepo";}
    #];

    ### You can pin a specific commit setting commit=<hash> attribute.
    packages = [ # FLATPAKS MANAGER
      { appId = "app/com.github.tchx84.Flatseal/x86_64/stable"; origin = "flathub";  }

      # DBEAVER
      { appId = "app/io.dbeaver.DBeaverCommunity/x86_64/stable"; origin = "flathub";  }
      { appId = "runtime/io.dbeaver.DBeaverCommunity.Client.pgsql/x86_64/stable"; origin = "flathub";  }
      { appId = "runtime/io.dbeaver.DBeaverCommunity.Client.mariadb/x86_64/stable"; origin = "flathub";  }

      # GIMP
      { appId = "app/org.gimp.GIMP/x86_64/stable"; origin = "flathub";  }
      { appId = "runtime/org.gimp.GIMP.Plugin.Fourier/x86_64/2-40"; origin = "flathub";  }
      { appId = "runtime/org.gimp.GIMP.Plugin.FocusBlur/x86_64/2-40"; origin = "flathub";  }
      { appId = "runtime/org.gimp.GIMP.Plugin.GMic/x86_64/2-40"; origin = "flathub";  }
      { appId = "runtime/org.gimp.GIMP.Plugin.Resynthesizer/x86_64/2-40"; origin = "flathub";  }
      { appId = "runtime/org.gimp.GIMP.Plugin.BIMP/x86_64/2-40"; origin = "flathub";  }
      { appId = "runtime/org.gimp.GIMP.Plugin.LiquidRescale/x86_64/2-40"; origin = "flathub";  }
      { appId = "runtime/org.gimp.GIMP.Manual/x86_64/2.10"; origin = "flathub";  }
      { appId = "runtime/org.gimp.GIMP.Plugin.Lensfun/x86_64/2-40"; origin = "flathub";  }

      # PRODUCTIVITY
      { appId = "app/net.ankiweb.Anki/x86_64/stable"; origin = "flathub";  }
      { appId = "app/md.obsidian.Obsidian/x86_64/stable"; origin = "flathub";  }

      # UNRELATED
      #{ appId = "org.ferdium.Ferdium"; origin = "flathub";  }
      #{ appId = "app/io.github.dvlv.boxbuddyrs"; origin = "flathub";  }
      #{ appId = "page.codeberg.JakobDev.jdDesktopEntryEdit"; origin = "flathub";  }
      #{ appId = "app/com.github.Murmele.Gittyup/x86_64/stable"; origin = "flathub";  }
      { appId = "app/org.onlyoffice.desktopeditors/x86_64/stable"; origin = "flathub";  }

     # # UNEEDED ON NIX...
     # #{ appId = "x.y.z"; origin = "flathub";  }
    ];
  };

  customOPTS.filesystems.persist = {
    home.directories = [".var/app" ".local/share/flatpak"];
    # home_cache.directories = [".cache/flatpak"];
  };
}
