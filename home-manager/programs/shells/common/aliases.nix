{config, ...}: {
  home.shellAliases = {
    ".." = "cd ..";
      man = "batman";
      gnix = "cd $HOME/storageHub/.YOS/nix";
      vnix = "cd $HOME/storageHub/.YOS/nix; vim $HOME/storageHub/.YOS/nix";

      gpenv = "cd $HOME/storageHub/.YOS/penv";
      upenv = "cd $HOME/storageHub/.YOS/nix && nix flake lock --update-input penv";
      vpenv = "cd $HOME/storageHub/.YOS/penv; vim $HOME/storageHub/.YOS/penv";

      gp = "git -C \"$HOME/storageHub/.YOS/nix/\" pull";
      ga = "git -C \"$HOME/storageHub/.YOS/nix/\" add \"$HOME/storageHub/.YOS/nix/.\"";

      ls = "eza --icons=always";
      la = "eza --icons=always -a";
      ll = "eza --icons=always -lh";
      l1 = "eza --icons=always -1tr";
  };
}
