{
  config,
  inputs,
  ...
}: {
  home.file.".gitconfig" = {
    source = "${inputs.penv}/config/user/git/.gitconfig";
  };
}
