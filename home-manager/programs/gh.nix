{
  config,
  lib,
  pkgs,
  ...
}: lib.mkMerge [
  {
    programs.gh.enable = true;
  }

  (lib.mkIf (config.customOPTS.programs.sops.enable) {
    sops.secrets."gitforge_tokens/github" = {};
    programs.gh.package = pkgs.symlinkJoin {
      name = "gh";
      paths = [ pkgs.gh ];
      buildInputs = [pkgs.makeWrapper];
      postBuild = ''
          wrapProgram $out/bin/gh \
            --run 'export GH_TOKEN=$(cat ${config.sops.secrets."gitforge_tokens/github".path})'
        '';
      meta.mainProgram = "gh";
    };
  })
]
