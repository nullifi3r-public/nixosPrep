{ config, pkgs, lib, inputs, ... }:

{
  home.file.".config/tealdeer" = {
    recursive = true;
    executable = true;
    source = "${inputs.penv}/config/user/tealdeer";
  };

  home.packages = [ pkgs.tealdeer ];

  # customOPTS.filesystems.persist = {
  #   home_cache.directories = [".cache/tealdeer"];
  # };
}
