{
  config,
  inputs,
  pkgs,
  ...
}: {
  home.packages = [pkgs.custom.chromaterm];
  #home.packages = [(inputs.nixpkgs2305.legacyPackages.${pkgs.system}.callPackage ../../packages/chromaterm {})];
  home.file.".chromaterm.yml" = {
    source = "${inputs.penv}/config/user/chromaterm/.chromaterm.yml";
  };
}
