{
  config,
  inputs,
  user,
  ...
}: {
  # home.file.".config/kitty" = {
  #   source = "${inputs.penv}/config/user/kitty";
  #   recursive = true;
  # };


  programs.kitty = { # ENABLED HERE, SO THAT STYLIX WILL PICK IT UP
    enable = true;
    extraConfig = builtins.readFile "${inputs.penv}/config/user/kitty/kitty.conf";
  };
}
