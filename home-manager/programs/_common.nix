{
  config,
  isNixOS,
  lib,
  machineType,
  pkgs,
  ...
}:
{
  # ADD CUSTOM USER CREATED SHELL PACKAGES TO pkgs.custom.shell
  nixpkgs.overlays = lib.mkIf (!isNixOS) [
    (_: prev: {
      custom = (prev.custom or { }) // {
        shell = config.customOPTS.programs.shell.finalPackages;
      };
    })
  ];

  services.network-manager-applet.enable = lib.mkIf (config.customOPTS.desktops.hyprland.enable) true;

  home.packages = with pkgs;
  # UNIVERSAL PKGS
  [
    ## SHELL UTILS
    bottom
    btop
    brightnessctl
    cmatrix
    expect
    eza
    fd
    fzf
    gdu
    htop
    k9s kdash
    lazydocker
    lazygit
    p7zip
    rar # INCLUDES UNRAR
    ripgrep
    sshpass  autossh
    xplr yazi
    yt-dlp
    zellij
    zoxide
  
    # NIX-RELATED
    ## NIX COMMAND IMPROVERS
    nix-output-monitor
    nvd

    ## PACKAGING
    nurl # GENERATE FETCHERS # https://github.com/nix-community/nurl
    nix-init # GENERATE PACKAGES # https://github.com/nix-community/nix-init
    nvfetcher # UPDATE GENERATED CONFIGS
    
    ## NIX LANGUAGE SUPPORT
    alejandra nixfmt-rfc-style nixpkgs-fmt # FORMATTERS
    nixd # LSP
  ]
  ++ (lib.optional (!isNixOS) nh)

  # ADD CUSTOM USER CREATED SHELL PACKAGES
  ++ (lib.attrValues config.customOPTS.programs.shell.finalPackages)

  # DESKTOP-ONLY PKGS
  ++ lib.optionals (machineType != "server") [
    aspell hunspell
    bibata-cursors
    copyq
    d-spy
    flatpak
    gittyup
    keepassxc keepmenu
    kitty
    bash-language-server
    nordzy-icon-theme adwaita-icon-theme
    peazip
    qalculate-gtk
    shellharden
    smile # BETTER EMOJI PICKER

    ## MULTIMEDIA
    scrcpy
    ### IMAGES
    nomacs
    ### VIDEO
    smplayer
    mpv
  ]

  # HYPRLAND-ONLY PKGS
  ++ lib.optionals (config.customOPTS.desktops.hyprland.enable) [
    # THEMING
    kdePackages.qt5compat
    kdePackages.qt6ct
    lxappearance

    # SCREENSHOTTING
    grim
    grimblast
    satty
    swappy

    # WALLPAPERS
    swww
    waypaper

    # DISPLAY/SCREEN MANAGEMENT
    wluma
    ydotool

    # CLIPBOARD
    cliphist
    wl-clipboard
  ]

  # SERVER-ONLY PKGS
  ++ lib.optionals (machineType != "server") [
  ]

  # SPECIAL-CASE PKGS
  ## KDE APPS ON NON KDE DEKSTOPS (ALWAYS NEEDED ON desktop MACHINES)
  ++ lib.optionals (machineType == "desktop" && !config.customOPTS.desktops.plasma.enable) [
    kdePackages.ark
    kdePackages.dolphin
    kdePackages.dolphin-plugins
    kdePackages.okular
    #kdePackages.gwenview # USING nomacs NOW
  ]

  ## home-manager executable only on nixos
  ++ (lib.optional (isNixOS) home-manager)

  ## PKGS BETTER TO BE ONLY WITH HM ON NIXOS
  ++ (lib.optional (isNixOS) ffmpeg)

  ## GAMING
  ### STEAM
  ++ (lib.optional (config.customOPTS.programs.gaming.steam.enable) steam)
  ### PROTONUP-NG
  ++ (lib.optional (config.customOPTS.programs.gaming.protonup-ng.enable) protonup-ng)
  ### LUTRIS
  ++ (lib.optional (config.customOPTS.programs.gaming.lutris.enable) (lutris.override {extraPkgs = pkgs: [wineWowPackages.wayland winetricks];}));

  customOPTS.filesystems.persist = {
    home.directories = [
      ".config/nomacs"
      ".config/peazip"
    ];
  };
}
