{
  config,
  pkgs,
  ...
}: {
  programs.bat = {
    enable = true;
    extraPackages = with pkgs.bat-extras; [ batdiff batman batgrep ];
    package = pkgs.bat;
    #syntaxes = {};
    #themes = {};

     config = {
       map-syntax = [
         "*.jenkinsfile:Groovy"
         "jenkinsfile:Groovy"
         "jenkinsFile-*:Groovy"
         "*.props:Java Properties"
       ];
       #theme = "TwoDark";
     };
  };
}
