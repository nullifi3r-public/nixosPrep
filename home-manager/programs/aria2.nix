{
  config,
  inputs,
  lib,
  isNixOS,
  pkgs,
  user,
  ...
}: let 
  cacheLocation = if ((isNixOS) && (lib.strings.hasInfix "impermanence" config.customOPTS.filesystems.osFS.format))
    then "/ps_home/cache/home/${user}/.cache/.aria2cd/.aria2.session"
    else "/home/${user}/.cache/.aria2cd/.aria2.session";
in {
  home = {
    activation = {
      GenerateAriaSessionFile = lib.hm.dag.entryAfter ["writeBoundary"] ''
        if [ ! -f ${cacheLocation} ]; then
          mkdir -p $(dirname ${cacheLocation}) && touch ${cacheLocation}
        fi
      '';
    };

    file.".config/aria2" = {
      recursive = true;
      source = "${inputs.penv}/config/user/aria2";
    };

    packages = [ pkgs.aria2 ];
  };

  systemd.user.services.aria2cd = {
    Unit = {
      Description = "Aria2 Download Manager Daemon Mode";
      ConditionPathExists = [
        "${cacheLocation}"
        "/home/${user}/.config/aria2/aria2.conf"
        ];
      After="network.target";
    };

    Service = {
      Type = "forking";
      ExecStart = " ${pkgs.aria2}/bin/aria2c --conf-path=/home/${user}/.config/aria2/aria2.conf --input-file=${cacheLocation} --save-session=${cacheLocation}";
    };

    Install = {WantedBy = [ "default.target" ];};
  };

  # customOPTS.filesystems.persist = {
  #   home_cache.directories = [".cache/.aria2cd"];
  # };
}
