{
  config,
  inputs,
  isNixOS,
  lib,
  machineType,
  ...
}: lib.mkMerge [

  {

    # programs.ssh.enable = true; # DON'T' ENABLE UNLESS YOU WILL CONFIGURE IT USING NIX!
    services.ssh-agent.enable = lib.mkIf (!isNixOS) true;

    home = {
      file.".ssh" = {
        #source = config.lib.file.mkOutOfStoreSymlink "${inputs.penv}/config/user/ssh";
        source = "${inputs.penv}/config/user/ssh";
        recursive = true;
      };
      #sessionVariables.SSH_AUTH_SOCK = "/run/user/1000/ssh-agent";
    };

  }

  (lib.mkIf (machineType == "vm") { customOPTS.programs.shell.packages.dk = ''
      if [ -f "$HOME/.config/sops/ssh/id_ed25519" ]; then
        ssh-add "$HOME/.config/sops/ssh/id_ed25519"
      else
        echo -e "\nKEY IS NOT FOUND IN THE EXPECTED DIRECTORY. ADD IT MANUALLY...\n"
      fi
    '';
  })
]