{
  config,
  inputs,
  lib,
  pkgs,
  ...
}: lib.mkIf (config.customOPTS.programs.swaync.enable) {
  services.swaync = {
    enable = true;
    package = pkgs.swaynotificationcenter;
  };
  
  home = {
    packages = [ pkgs.swaynotificationcenter ];
    file.".config/swaync" = {
      recursive = true;
      source = "${inputs.penv}/config/user/swaync";
    };
  };
}
