{
  config,
  lib,
  machineType,
  ...
}: lib.mkIf (machineType == "desktop") {
  services.kdeconnect = {
    enable = true;
    indicator = true;
  };
}
