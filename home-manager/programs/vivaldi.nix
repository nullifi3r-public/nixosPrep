{
  config,
  lib,
  machineType,
  pkgs,
  ...
}:
lib.mkIf ((machineType != "server") && (config.customOPTS.programs.browser == "vivaldi")) {

  #programs.chromium = {
  #  enable = true;
  #  package = pkgs.vivaldi.override { proprietaryCodecs = true; enableWidevine = true;};
  #};

  home = {
    sessionVariables = { 
      DEFAULT_BROWSER = lib.getExe pkgs.vivaldi;
      BROWSER = lib.getExe pkgs.vivaldi;
    };
    packages = with pkgs; [
      (vivaldi.override { proprietaryCodecs = true; enableWidevine = true;})
      libsForQt5.qt5.qtbase
      vivaldi-ffmpeg-codecs
    ];
  };

  xdg.mimeApps.defaultApplications = {
    #"text/html"                 = "vivaldi-stable.desktop";
    "x-scheme-handler/http"     = "vivaldi.desktop";
    "x-scheme-handler/https"    = "vivaldi.desktop";
    "x-scheme-handler/about"    = "vivaldi.desktop";
    "x-scheme-handler/unknown"  = "vivaldi.desktop";
  };

  customOPTS.filesystems.persist = {
    home.directories = [".config/vivaldi" ".local/lib/vivaldi"];
    home.files = [".local/share/.vivaldi_reporting_data"];
    # home_cache.directories = [".cache/vivaldi"];
  };
}
