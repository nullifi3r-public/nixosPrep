{
  config,
  inputs,
  lib,
  pkgs,
  ...
}: lib.mkIf (config.customOPTS.programs.waybar.enable) {
  programs.waybar = {
    enable = true;
    package = pkgs.waybar;
  };
  
  home = {
    packages = [pkgs.waybar];
    file.".config/waybar" = {
      recursive = true;
      source = "${inputs.penv}/config/user/waybar";
    };
  };
}
