{
  config,
  inputs,
  lib,
  pkgs,
  ...
}:
lib.mkIf (config.customOPTS.programs.atuin.enable) {
  programs.atuin = {
    enable = true;
    package = pkgs.atuin;
    enableBashIntegration = true;
    enableFishIntegration = true;
    enableZshIntegration = true;
    flags = [
      "--disable-up-arrow"
      #"--disable-ctrl-r"
    ];
    
    # FOR CONFIGURATION SETTINGS, SEE https://docs.atuin.sh/configuration/config/
    # settings = [];
  };
  home = {
    packages = [pkgs.atuin];
    file.".config/atuin/config.toml" = {
      source = "${inputs.penv}/config/user/atuin/config.toml";
    };
  };
}
