{
  config,
  inputs,
  ...
}: {
  home.file.".local/bin/kxc-unlock" = {
    executable = true;
    source = "${inputs.penv}/config/user/keepassxc/scripts/kxc-unlock";
  };

  home.file.".local/bin/kxc-init" = {
    executable = true;
    source = "${inputs.penv}/config/user/keepassxc/scripts/kxc-init";
  };
}
