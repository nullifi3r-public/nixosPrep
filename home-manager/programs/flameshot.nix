{
  config,
  pkgs,
  lib,
  ...
}:
lib.mkIf (config.customOPTS.programs.flameshot.enable) {
  services.flameshot = {
    enable = true;
    package = pkgs.flameshot;
    settings = {
      General = {
        disabledTrayIcon = false;
        showStartupLaunchMessage = false;
      };
    };
  };

  home.packages = [pkgs.flameshot];
}
