{config, ...}: {
  programs = {
    yt-dlp = {
      enable = true;
      settings = {
        add-metadata = true;
        format = "best[ext=webm]";
        no-mtime = true;
        output = "%(title)s.%(ext)s";
        sponsorblock-mark = "all";
        windows-filenames = true;
      };
    };
  };

  home.shellAliases = {
    yt = "yt-dlp";
    ytaudio = "yt-dlp --audio-format mp3 --extract-audio";
    ytsub = "yt-dlp --write-auto-sub --sub-lang='en,eng' --convert-subs srt";
    ytplaylist = "yt-dlp --output '%(playlist_index)d - %(title)s.%(ext)s'";
  };
}
