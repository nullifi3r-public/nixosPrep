{ config, pkgs, lib, isNixOS, ... }:
{
  imports = [
    ./shells

    ./_common.nix

    ./aria2.nix
    ./atuin.nix
    ./bat.nix
    ./chromaterm.nix
    ./flameshot.nix
    ./flatpak.nix
    ./gh.nix
    ./gitconfig.nix
    ./kde-connect.nix
    ./keepassxc.nix
    ./kitty.nix
    ./nextcloud.nix
    ./ssh-client.nix
    ./swaync.nix
    ./tealdeer.nix
    ./virtmanager.nix
    ./vivaldi.nix
    ./waybar.nix
    ./yt-dlp.nix
  ];
}
