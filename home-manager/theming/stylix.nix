## YOU CAN USE A SPECIFIC COLOR WHEN NEEDED. E.G.:
#wayland.windowManager.hyprland.settings.general."col.active_border" = lib.mkForce "rgb(${config.stylix.base16Scheme.base0E})";
{
  config,
  lib,
  machineType,
  ...
}: lib.mkIf (config.customOPTS.theming.stylix.enable && machineType != "server") {
  stylix = (config.customOPTS.theming.stylix.commonStylixConfig // {
    # WHETHER TO ENABLE TARGETS BY DEFAULT. WHEN THIS IS FALSE, ALL TARGETS ARE DISABLED UNLESS EXPLICITLY ENABLED
    autoEnable = true;
  });
}