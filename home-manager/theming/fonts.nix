{
  config,
  isNixOS,
  lib,
  pkgs,
  ...
}: {
  home.packages = lib.optionals (!isNixOS) config.customOPTS.theming.fonts.packages;

  # setup fonts for user-scope only, run "fc-cache -f" to refresh fonts
  fonts = {
    fontconfig = {
      enable = true;
      defaultFonts = lib.mkIf (!config.customOPTS.theming.stylix.enable) {
        emoji = ["Twitter Color Emoji" "Noto Color Emoji"];
        monospace = ["Jetbrainsmono Nerd Font Mono" "Twitter Color Emoji" "Noto Color Emoji"];
        sansSerif = ["SF Pro Display" "Noto Sans" "Noto Kufi Arabic" "Ubuntu Nerd Font Propo" "Twitter Color Emoji" "Noto Color Emoji"];
        serif = ["SF Pro Display" "Noto Serif" "Noto Kufi Arabic" "Ubuntu Nerd Font Propo" "Twitter Color Emoji" "Noto Color Emoji"];
      };
    };
  };
}
