{config, ...}: {
  imports = [
    ./fonts.nix
    ./stylix.nix
  ];

  customOPTS.filesystems.persist = {
    home.directories = [
      # FONTS
      ".config/fontconfig"
      
      # THEMES
      ".config/gtk-2.0"
      ".config/gtk-3.0"
      ".config/gtk-4.0"
      ".icons"
      ".local/share/icons"
      ".local/share/themes"
      ".themes"
    ];
    home.files = [
      ".config/gtkrc-2.0"
      ".config/gtkrc"
      ".gtkrc-2.0"
      ".gtkrc"
    ];
  };
}
