{
  config,
  isNixOS,
  lib,
  pkgs,
  user,
  ...
}:{
  home = {
    username = user;
    homeDirectory = "/home/${user}";

    stateVersion = "24.05";

    sessionVariables = {
      __IS_NIXOS = if (isNixOS) then "1" else "0";
      NIXPKGS_ALLOW_UNFREE = "1";
    };

  };
  
  # DEAL BETTER WITH LEGACY OSes (Non NixOS)
  targets.genericLinux.enable = lib.mkIf (!isNixOS) true;
  
  # LET HOME MANAGER INSTALL AND MANAGE ITSELF.
  programs.home-manager.enable = true;
}