{...}:
{
  imports = [
    ./desktops
    ./programs
    ./theming

    ./bluetooth.nix
    ./home-manager.nix
    ./xdg.nix
  ];
}
