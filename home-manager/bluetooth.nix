{
  config,
  pkgs,
  lib,
  ...
}: lib.mkMerge [
  {
    services.mpris-proxy.enable = true;
  }

  (lib.mkIf (config.customOPTS.desktops.hyprland.enable) {
    home.packages = [pkgs.blueman];
    services.blueman-applet.enable = true;

  })
]
