{ # xdg-mime query default application/zip
  config,
  user,
  ...
}: {
  xdg = {
    enable = true;

    mimeApps.enable = true;

    userDirs = {
      enable = true;
      createDirectories = true;
      desktop         = "/home/${user}/storageHub/transit";
      documents       = "/home/${user}/storageHub/media/documents";
      download        = "/home/${user}/storageHub/network/_downloads/local_downloads";
      music           = "/home/${user}/storageHub/media/audio";
      pictures        = "/home/${user}/storageHub/media/pictures";
      publicShare     = "/home/${user}/.local/misc/Public";
      templates       = "/home/${user}/.local/misc/Templates";
      videos          = "/home/${user}/storageHub/media/video";
    };
  };
}
