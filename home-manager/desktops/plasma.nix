{
  config,
  lib,
  ...
}: lib.mkIf (config.customOPTS.desktops.plasma.enable) {


  customOPTS.filesystems.persist = {
    home.directories = [
      ".config/kde.org"
      ".config/KDE"
      ".config/kdedefaults"
      ".config/session"
      ".local/share/baloo"
      ".local/share/dolphin"
      ".local/share/kate"
      ".local/share/kio"
      ".local/share/klipper"
      ".local/share/knewstuff3"
      ".local/share/konsole"
      ".local/share/krdpserver"
      ".local/share/kwalletd"
      ".local/share/kwrite"
      ".local/share/servicemenu-download"
      ".local/share/systemsettings"
      ".local/share/xdg-desktop-portal-kde"
    ];
    home.files = [
      ".config/baloofileinformationrc"
      ".config/baloofilerc"
      ".config/dolphinrc"
      ".config/katemetainfos"
      ".config/katerc"
      ".config/katevirc"
      ".config/kconf_updaterc"
      ".config/kded5rc"
      ".config/kded6rc"
      ".config/kdeglobals"
      ".config/kglobalshortcutsrc"
      ".config/kiorc"
      ".config/konsolerc"
      ".config/konsolesshconfig"
      ".config/krdpserverrc"
      ".config/kservicemenurc"
      ".config/ktrashrc"
      ".config/kwalletrc"
      ".config/kwinoutputconfig.json"
      ".config/kwinrc"
      ".config/plasma-org.kde.plasma.desktop-appletsrc"
      ".config/plasmarc"
      ".config/plasmashellrc"
      ".config/powerdevilrc"
      ".config/powermanagementprofilesrc"
      ".config/Trolltech.conf"
      ".config/xdg-desktop-portal-kderc"
      ".local/share/krunnerstaterc"
      ".local/share/recently-used.xbel"
    ];
  };
}
