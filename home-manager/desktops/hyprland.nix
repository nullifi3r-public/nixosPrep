{
  config,
  inputs,
  lib,
  pkgs,
  ...
}: lib.mkIf (config.customOPTS.desktops.hyprland.enable) {

  home = {
    file = lib.mkIf (config.customOPTS.desktops.hyprland.enable) {
      ".config/hypr" =  {source = "${inputs.penv}/config/user/hyprland"; recursive = true;};
    };
    packages = with pkgs;
      [hyprland]
      ++ (lib.optional (config.customOPTS.programs.hyprlock.enable) hyprlock)
      ++ (lib.optional (config.customOPTS.programs.hypridle.enable) hypridle)
    ;
  };

  programs.hyprlock = lib.mkIf (config.customOPTS.programs.hyprlock.enable) {
    enable = true;
    package = pkgs.hyprlock;
  };

  wayland.windowManager.hyprland = {
    enable = true;
    package = pkgs.hyprland;
    xwayland.enable = true;
    # plugins = [
    #   hyprplugins.hyprtrails
    # ];
    systemd = {
      enable = false;
      enableXdgAutostart = false;
      # variables = ["--all"]; #["DISPLAY" "HYPRLAND_INSTANCE_SIGNATURE" "WAYLAND_DISPLAY" "XDG_CURRENT_DESKTOP"];
      # extraCommands = [
      #   "systemctl --user stop hyprland-session.target"
      #   "systemctl --user start hyprland-session.target"
      # ];
    };
  };

  xdg.portal = {
    enable = true;
    extraPortals = [
      pkgs.xdg-desktop-portal-gtk
      pkgs.xdg-desktop-portal
    ];
    configPackages = [
      pkgs.xdg-desktop-portal-gtk
      pkgs.xdg-desktop-portal-hyprland
      pkgs.xdg-desktop-portal
    ];
  };

  # customOPTS.filesystems.persist.home.directories = [".config/hypr"]; # NOT-NEEDED SINCE HOME-MANAGER ALREADY SOURCE MY CONFIG FROM penv INPUT
}
