{
  config,
  inputs,
  lib,
  pkgs,
  ...
}:
{
  nixpkgs.overlays = [
    (_: prev: {
      # include nixpkgs stable
      stable = import inputs.nixpkgs-stable {inherit (prev.pkgs) system; config.allowUnfree = true;};
      
      # include custom packages
      custom = (prev.custom or { }) // (import ../packages {inherit (prev) pkgs; inherit inputs;});

      # Workaround for Pynose Issue. Fixed by https://github.com/NixOS/nixpkgs/issues/311054
      #python312 = prev.python312.override { packageOverrides = _: pysuper: { nose = pysuper.pynose; }; };
      #python312 = prev.python312.override { packageOverrides = _: pysuper: { xlib = pysuper.xlib.overridePythonAttrs { doCheck = false; }; };};
    })
  ];
}
