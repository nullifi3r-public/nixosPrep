#!/usr/bin/env bash
#######
# TASKS
#######
# TASK: 

# set -euo pipefail
set -o errexit  # EXIT ON ERROR
set -o nounset  # TREAT UNSET VARIABLES AS ERRORS
set -o pipefail # PROPAGATE EXIT CODES THROUGH PIPES. ENSURES THE SCRIPT EXITS WITH A NON-ZERO STATUS IF ANY COMMAND WITHIN A PIPE CHAIN RETURNS A NON-ZERO EXIT CODE

## CHECK IF THE NUMBER OF NEEDED ARGUMENTS IS NOT AS EXPECTED
if [ "$#" -ne 2 ]; then
    echo -e "\n\nThis script expects exactly 2 arguments:\n\t- ARG#1: Name of Host. e.g.: g50, test-vm, ..., etc.\n\t- ARG#2: Name of the Disko file to use as the Partitioning flow. e.g.: btrfs.nix, btrfs-impermanence.nix, ..., etc\n"
    exit 1
fi


#######
# VARS
#######
HOST_TO_INSTALL="$1"
DISKO_FILE_NAME="$2"
REPO_BRANCH="hyprland"
REPO_URL="git@gitlab.com:nullifi3r-public/nixosPrep.git"
read -srp 'Enter Decryption Password: ' DECRYPTION_PASSWORD

echo -e "\n\n"

# OPTIONAL UPDATE OF THE FLAKE LOCK-FILE
read -rp 'Do You Want To Update Your Flake Inputs: (YyNn) ' UPDATE_FLAKE_INPUTS_PROMPT
case $UPDATE_FLAKE_INPUTS_PROMPT in
    [Yy]*)
        UPDATE_FLAKE_INPUTS="true"
        ;;
    *)
        UPDATE_FLAKE_INPUTS="false"
        ;;
esac

#########
# CHECKS
#########
if [ -d "./hosts" ]; then
    LIST_OF_HOSTS=$(find ./hosts -mindepth 2 -maxdepth 2 -type d -not -name '.*' -printf '%f\n' | paste -sd '|')
else
    echo -e "\nCOULDN'T FIND THE './hosts' DIRECTORY. ABORTING...\n"
    exit 2
fi

## CHECK IF THE PROVIDED HOST NAME AVAILABLE IN THE HOSTS DIRECTORY
if [[ ! "$HOST_TO_INSTALL" =~ ($LIST_OF_HOSTS) ]]; then
    echo -e "\nInvalid First Argument. The Provided Host Name Could not be found among the defined hosts in the target flake. Please Recheck...\n"
    exit 1
fi

# SINCE THE hosts DIRECTORY IS AVAILABLE && THE PROVIDED HOSTNAME EXIST, GET THE PATH TO IT'S DIRECTORY (TO AVODI MACHINE-TYPE SPECIFICATION)
HOST_TO_INSTALL_PATH=$(find "./hosts" -type d -name "$HOST_TO_INSTALL")

# USING THE GENERAL DISKO CONFIGURATION FOR VMs AND HOST-SPECIFC ONES OTHERWISE
if [[ "$HOST_TO_INSTALL" == *-vm* ]]; then
  DISKO_FILE_PATH="./hosts/vm/_common/disko/$DISKO_FILE_NAME"
  ## CHECK IF THE SPECIFIED DISKO FILE NAME EXIST
  if [ ! -f "$DISKO_FILE_PATH" ]; then
      echo -e "\nInvalid Second Argument. The Provided Disko File Name Could Not Be Found. Please Recheck..."
      exit 1
  fi
else
  DISKO_FILE_PATH="$HOST_TO_INSTALL_PATH/disko/$DISKO_FILE_NAME"
  ## CHECK IF THE SPECIFIED DISKO FILE NAME EXIST
  if [ ! -f "$DISKO_FILE_PATH" ]; then
      echo -e "\nInvalid Second Argument. The Provided Disko File Name Could Not Be Found. Please Recheck..."
      exit 1
  fi

fi

# DETERMINING AGE KEY LOCATION
if [[ "$DISKO_FILE_NAME" == *impermanence* ]]; then
    SOPS_CONFIG_LOCATION="/mnt/ps_home/home/yahia/.config"
else
    SOPS_CONFIG_LOCATION="/mnt/home/yahia/.config"
fi

# DECRYPTING SSH KEY
ssh -t root@nixos-iso.local << EOF
mkdir -p /root/.ssh/ && chmod 700 /root/.ssh
openssl enc -d -aes-256-cbc -a -pbkdf2 -in /etc/encryptedSecrets/YUniKey.enc -out /root/.ssh/id_ed25519 -pass pass:"$DECRYPTION_PASSWORD" && chmod 600 /root/.ssh/id_ed25519
EOF


# ADDING SSH KEY TO AGENT
ssh -t root@nixos-iso.local << SSH
expect << EOF
spawn ssh-add /root/.ssh/id_ed25519
expect "Enter passphrase for /root/.ssh/id_ed25519:"
send "$DECRYPTION_PASSWORD\r"
expect eof
EOF
SSH


# PULLING YOS FLAKE REPO
ssh -t root@nixos-iso.local << EOF

rm -rf /tmp/env && mkdir -p /tmp/env && git clone -b "$REPO_BRANCH" "$REPO_URL" /tmp/env && cd /tmp/env/

if [ "$UPDATE_FLAKE_INPUTS" = "true" ]; then
    nix flake update -v
fi

nixos-generate-config --show-hardware-config --no-filesystems > /tmp/env/"$HOST_TO_INSTALL_PATH"/hardware.nix

git -C /tmp/env/ -c user.name="Nullifi3r" -c user.email="tools.e@outlook.com" add /tmp/env/.
if git diff --cached --exit-code; then
    echo "No changes to commit. Proceeding..."
else
    git -C /tmp/env/ -c user.name="Nullifi3r" -c user.email="tools.e@outlook.com" commit -m "UPGRADE: FLAKE INPUTS UPGRADED FROM AN INSTALLATION PROCCESS FOR \"$HOST_TO_INSTALL\" MACHINE"
    git -C /tmp/env/ -c user.name="Nullifi3r" -c user.email="tools.e@outlook.com" push
fi

disko --mode disko "$DISKO_FILE_PATH"

mkdir -p "$SOPS_CONFIG_LOCATION/sops/age/"
openssl enc -d -aes-256-cbc -a -pbkdf2 -in /etc/encryptedSecrets/nixAgeKey.enc -out "$SOPS_CONFIG_LOCATION"/sops/age/nix.txt -pass pass:"$DECRYPTION_PASSWORD"
if [[ "$HOST_TO_INSTALL" == *-vm* ]]; then
  mkdir -p "$SOPS_CONFIG_LOCATION/sops/ssh/"
  openssl enc -d -aes-256-cbc -a -pbkdf2 -in /etc/encryptedSecrets/YUniKey.enc -out "$SOPS_CONFIG_LOCATION"/sops/ssh/id_ed25519 -pass pass:"$DECRYPTION_PASSWORD" && chmod 600 "$SOPS_CONFIG_LOCATION"/sops/ssh/id_ed25519
fi
chown -R 1000:100 "$SOPS_CONFIG_LOCATION"

if [[ "$DISKO_FILE_NAME" == *impermanence* ]]; then
    chown -R 1000:100 /mnt/ps_home/home
fi

EOF

## STARTING A TMUX SESSION
ssh root@nixos-iso.local "tmux new-session -d -s 'installer'"

## CHECKING IF PREVIOUS COMMANDS WORKED AS EXPECTED
ssh root@nixos-iso.local "tmux send-keys -t 'installer' \
\"export SAFE_TO_INSTALL='false'; \
if [ -f '/root/.ssh/id_ed25519' ] && [ -s '/root/.ssh/id_ed25519' ]; then \
  export SAFE_TO_INSTALL='true'; \
else \
  export SAFE_TO_INSTALL='false'; echo 'SSH KEY IS EITHER EMPTY OR NOT FOUND!'; \
fi; \
if [ -d '/tmp/env/.git' ]; then \
  export SAFE_TO_INSTALL='true'; \
else \
  export SAFE_TO_INSTALL='false'; echo 'FLAKE GIT REPO NOT FOUND'; \
fi; \
if [ -d '/mnt/boot' ]; then \
  export SAFE_TO_INSTALL='true'; \
else \
  export SAFE_TO_INSTALL='false'; echo 'DISK LAYOUT IN /mnt IS NOT AS EXPECTED'; \
fi; \
if [ -f $SOPS_CONFIG_LOCATION/sops/age/nix.txt ] && [ -s $SOPS_CONFIG_LOCATION/sops/age/nix.txt ]; then \
  export SAFE_TO_INSTALL='true'; \
else \
  export SAFE_TO_INSTALL='false'; echo 'AGE KEY IS EITHER EMPTY OR NOT FOUND!'; \
fi; \
\" C-m"


## CHECK THE EXISTENCE OF A LOCAL CACHE && INSTALLATION
ssh root@nixos-iso.local "tmux send-keys -t 'installer' \
\"if [ \\\$SAFE_TO_INSTALL = 'true' ]; then \
  if ping -c 10 -W 15 'cache-vm.local' > /dev/null 2>&1; then \
    clear; echo -e '\nSTARTING THE INSTALLTION PROCESS USING LOCAL CACHE @ cache-vm MACHINE ...\n\n\n'; nixos-install --option extra-substituters 'http://cache-vm.local:5000' --no-root-password --root /mnt --flake /tmp/env/.#$HOST_TO_INSTALL; \
    echo -e '\nPushing Local Nix Store to cache server\n' ; nix copy --all --to ssh://yahia@cache-vm.local; \
  else \
    if ping -c 10 -W 15 'toshiba.local' > /dev/null 2>&1; then \
      clear; echo -e '\nSTARTING THE INSTALLTION PROCESS USING LOCAL CACHE @ toshiba MACHINE...\n\n\n'; nixos-install --option extra-substituters 'http://toshiba.local:5000' --no-root-password --root /mnt --flake /tmp/env/.#$HOST_TO_INSTALL; \
      echo -e '\nPushing Local Nix Store to cache server\n' ; nix copy --all --to ssh://yahia@toshiba.local; \
    else \
      clear; echo -e '\nSTARTING THE INSTALLTION PROCESS...\n\n\n'; nixos-install --no-root-password --root /mnt --flake /tmp/env/.#$HOST_TO_INSTALL; \
    fi;\
  fi; \
else \
    echo 'INSTALL HAS BEEN ABORTED DUE TO THE REASON(S) ABOVE.'; \
fi; \
\" C-m"


ssh -t root@nixos-iso.local "tmux a -t 'installer'"
