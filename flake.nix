{
  description = "YOS-NIX FLAKE CONFIGURATION";

  inputs = {
    # MAIN INPUTS
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable"; # NIXPKGS: UNSTABLE
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-24.05"; # NIXPKGS: STABLE: 24.05 | CHANGE WHEN SWITCHING TO NEW RELEASES
    home-manager = { url = "github:nix-community/home-manager"; inputs.nixpkgs.follows = "nixpkgs"; }; # HOME MANAGER: UNSTABLE

    # SYSTEM INPUTS
    disko = { url = "github:nix-community/disko"; inputs.nixpkgs.follows = "nixpkgs"; };
    impermanence.url = "github:nix-community/impermanence";
    stevenblack-hosts = { url = "github:StevenBlack/hosts"; flake = false; };

    # HOMEMANAGER INPUTS
    nix-flatpak.url = "github:gmodena/nix-flatpak";

    # GLOBAL INPUTS
    apple-fonts.url = "github:Lyndeno/apple-fonts.nix";
    sops-nix = { url = "github:Mic92/sops-nix"; inputs.nixpkgs.follows = "nixpkgs"; };
    stylix = { url = "github:danth/stylix"; inputs.nixpkgs.follows = "nixpkgs"; inputs.home-manager.follows = "home-manager"; };

    # PERSONAL INPUTS
    penv = { url = "git+ssh://git@gitlab.com/nullifi3r/YOS/penv.git?ref=main&shallow=1"; flake = false; };
    sopsVault = { url = "git+ssh://git@gitlab.com/nullifi3r/YOS/sopsVault.git?ref=main&shallow=1"; flake = false; };
  };

  outputs = inputs@{ nixpkgs, self, ... }: let
    system = "x86_64-linux";
    createCommonArgs = system: {
      inherit (nixpkgs) lib;
      inherit
        self
        inputs
        nixpkgs
        system
        ;
      pkgs = import inputs.nixpkgs {
        inherit system;
        config.allowUnfree = true;
      };
      specialArgs = {
        inherit self inputs;
      };
    };
    commonArgs = createCommonArgs system;
    forAllSystems =
      fn:
      nixpkgs.lib.genAttrs [
        "x86_64-linux"      # 64bit AMD/Intel x86
        #"aarch64-linux"    # 64bit ARM Linux
        #"x86_64-darwin"    # 64bit AMD/Intel macOS
        #"aarch64-darwin"   # 64bit ARM macOS
      ] (system: fn (createCommonArgs system));
  in {
    nixosConfigurations = (import ./hosts/nixos.nix commonArgs) // (import ./hosts/.iso commonArgs);
    homeConfigurations  = import ./hosts/home-manager.nix commonArgs;
    packages = forAllSystems (commonArgs': (import ./packages commonArgs'));
  };
}
#nix build ".#nixosConfigurations.$1.config.system.build.isoImage"
#nix build .#nixosConfigurations.yos.config.system.build.isoImage
# nix build --profile /nix/var/nix/profiles/system "/home/yahia/storageHub/.YOS/nix#nixosConfigurations.test-vm.config.system.build.toplevel"
# nix build --profile #homeConfigurations.test-vm.config.system.build
