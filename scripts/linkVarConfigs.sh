#!/usr/bin/env bash

## ASBRU
if [ -d "$HOME/storageHub/network/nextcloud/.apps/config/asbru" ]; then
    ln -sf "$HOME/storageHub/network/nextcloud/.apps/config/asbru" "$HOME/.config/asbru"
else
    echo -e "\nAsbru Source Configuration Not Found. Skipping...\n"
fi

## GitHub-CLI (gh)
if [ -d "$HOME/storageHub/network/nextcloud/.apps/config/gh" ]; then
    ln -sf "$HOME/storageHub/network/nextcloud/.apps/config/gh" "$HOME/.config/gh"
else
    echo -e "\nGitHub-CLI Source Configuration Not Found. Skipping...\n"
fi

## KeepassXC
if [ -d "$HOME/storageHub/network/nextcloud/.apps/config/keepassxc" ]; then
    ln -sf "$HOME/storageHub/network/nextcloud/.apps/config/keepassxc" "$HOME/.config/keepassxc"
else
    echo -e "\nKeepassXC Source Configuration Not Found. Skipping...\n"
fi

## SMPlayer
if [ -d "$HOME/storageHub/network/nextcloud/.apps/config/smplayer" ]; then
    ln -sf "$HOME/storageHub/network/nextcloud/.apps/config/smplayer" "$HOME/.config/smplayer"
else
    echo -e "\nSMPlayer Source Configuration Not Found. Skipping...\n"
fi

