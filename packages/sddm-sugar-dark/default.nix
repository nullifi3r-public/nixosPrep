{
  fetchFromGitHub,
  fetchurl,
  pkgs,
  stdenv,
  ...
}: {
  sddm-sugar-dark-theme = stdenv.mkDerivation rec {
    background = fetchurl {
      url = "https://raw.githubusercontent.com/NixOS/nixos-artwork/f07707cecfd89bc1459d5dad76a3a4c5315efba1/wallpapers/nix-wallpaper-nineish-dark-gray.png";
      sha256 = "9e1214b42cbf1dbf146eec5778bde5dc531abac8d0ae78d3562d41dc690bf41f";
    };
    src = fetchFromGitHub {
      owner = "MarianArlt";
      repo = "sddm-sugar-dark";
      rev = "v${version}";
      sha256 = "0gx0am7vq1ywaw2rm1p015x90b75ccqxnb1sz3wy8yjl27v82yhb";
    };
    pname = "sddm-sugar-dark-theme";
    version = "1.2";
    dontBuild = true;
    #buildInputs = with pkgs; [ gnused ];
    #nativeBuildInputs = [ wrapQtAppsHook ];
    propagatedUserEnvPkgs = with pkgs; [
      qtbase
      qtsvg
      qtgraphicaleffects
      qtquickcontrols2
    ];
    installPhase = ''
      mkdir -p $out/share/sddm/themes/sugar-dark/

      cp -afR $src/* $out/share/sddm/themes/sugar-dark/
      cp -afR $background $out/share/sddm/themes/sugar-dark/nix-bg.png

      sed -i 's/Background="Background.jpg"/Background="nix-bg.png"/g' $out/share/sddm/themes/sugar-dark/theme.conf
      sed -i 's/MainColor="navajowhite"/MainColor="#ebcb8b"/g' $out/share/sddm/themes/sugar-dark/theme.conf
      sed -i 's/AccentColor="white"/AccentColor="#e5e9f0"/g' $out/share/sddm/themes/sugar-dark/theme.conf
      sed -i 's/HH:mm/"h:mm AP"/g' $out/share/sddm/themes/sugar-dark/theme.conf
      sed -i 's/ForceHideCompletePassword=false/ForceHideCompletePassword=true/g' $out/share/sddm/themes/sugar-dark/theme.conf
      sed -i 's/HeaderText=Welcome!/HeaderText="Staaaaart (L)Nixing!"/g' $out/share/sddm/themes/sugar-dark/theme.conf
    '';
  };
}
