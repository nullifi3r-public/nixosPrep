{lib, pkgs, python311, fetchFromGitHub, ...}:
python311.pkgs.buildPythonApplication rec {
  pname = "chromaterm";
  version = "0.10.7";
  pyproject = true;

  src = fetchFromGitHub {
    owner = "hSaria";
    repo = "ChromaTerm";
    rev = version;
    hash = "sha256-IfaGaG/bBv/mPYVL3eBR2JYhnVOtLg4K7z5hjBvFsMo=";
  };

  nativeBuildInputs = with python311.pkgs; [
    setuptools
    wheel
    psutil
    pyyaml
  ];

  propagatedBuildInputs = with python311.pkgs;[ psutil pyyaml ];

  pythonImportsCheck = [ "chromaterm" ];

  meta = with lib; {
    description = "Color your Terminal with RegEx";
    homepage = "https://github.com/hSaria/ChromaTerm";
    license = licenses.mit;
    mainProgram = "ct";
  };
}