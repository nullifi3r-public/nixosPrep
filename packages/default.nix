{
  pkgs,
  ...
}:
let
  inherit (pkgs) lib callPackage;

  w =
    _callPackage: path: extraOverrides:
    let
      sources = pkgs.callPackages (path + "/generated.nix") { };
      firstSource = lib.head (lib.attrValues sources);
    in
    _callPackage (path + "/default.nix") (
      extraOverrides // { source = lib.filterAttrs (k: _: !(lib.hasPrefix "override" k)) firstSource; }
    );
in
{
  # USE w callPackage FOR nvFetcher-GENERATED PACKAGES AND REMOVE THE 'w' OTHERWISE
  chromaterm = callPackage ./chromaterm {};
  #asbru-cm = callPackage ./asbru-cm {};
  nomachine = callPackage ./nomachine {};
  distro-grub-themes-nixos = callPackage ./distro-grub-themes-nixos {};
}
